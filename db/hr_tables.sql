-- Table structure for table `vacancy`
CREATE TABLE `vacancy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `specialization` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `description` text,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Table structure for table `client`
CREATE TABLE `client` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `phone_number` varchar(25) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `password` varchar(50) NOT NULL,
  `is_blocked` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`,`role_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_client_role1_idx` (`role_id`),
  CONSTRAINT `fk_client_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Table structure for table `interview`
CREATE TABLE `interview` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('introductory','behavioral','technical','сase','group') NOT NULL,
  `status` enum('rejected','passed','appointed') NOT NULL,
  `comment` text,
  `subscriber_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_interview_subscriber1_idx` (`subscriber_id`),
  CONSTRAINT `fk_interview_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Table structure for table `role`
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` enum('admin','hr','aspirant') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Table structure for table `subscriber`
CREATE TABLE `subscriber` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_active` bit(1) DEFAULT b'1',
  `client_id` int(10) unsigned NOT NULL,
  `vacancy_id` int(10) unsigned NOT NULL,
  `status` enum('submitted','interview_appointed','job_offer','rejected') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_job_vacancy_has_user_job_vacancy1_idx` (`vacancy_id`),
  KEY `fk_subscriber_client1_idx` (`client_id`),
  CONSTRAINT `fk_job_vacancy_has_user_job_vacancy1` FOREIGN KEY (`vacancy_id`) REFERENCES `vacancy` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;