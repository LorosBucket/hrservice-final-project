-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: localhost    Database: hr
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `phone_number` varchar(25) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `password` varchar(50) NOT NULL,
  `is_blocked` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`,`role_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_client_role1_idx` (`role_id`),
  CONSTRAINT `fk_client_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'aspirant','Иван','Александрович','+43345432345',1,'1a1dc91c907325c69271ddf0c944bc72','\0'),(2,'hr','Ольга','Олеговна','+32532325232',2,'1a1dc91c907325c69271ddf0c944bc72','\0'),(3,'admin','',NULL,NULL,3,'1a1dc91c907325c69271ddf0c944bc72','\0'),(4,'Bob','Bobi','Markus','+32746897296',1,'1a1dc91c907325c69271ddf0c944bc72','\0'),(6,'loros','Vladislav','Dolnikov','+375336666040',1,'157efe861725f5917d88eea3e95a813a','\0'),(9,'marina','Марина','Абрамова ','+375298453927',2,'1a1dc91c907325c69271ddf0c944bc72','\0'),(10,'HrOksana','Оксана','Бодалян','+375228346737',2,'1a1dc91c907325c69271ddf0c944bc72','\0'),(11,'Arkhipov','Яков','Архипов','+375334390578',2,'1a1dc91c907325c69271ddf0c944bc72','\0'),(12,'Tatyana','Татьяна','Костюкова ','+375442978348',2,'1a1dc91c907325c69271ddf0c944bc72','\0'),(13,'hrtest','test','test','+375336666040',1,'81dc9bdb52d04dc20036dbd8313ed055','\0');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interview`
--

DROP TABLE IF EXISTS `interview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interview` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('introductory','behavioral','technical','сase','group') NOT NULL,
  `status` enum('rejected','passed','appointed') NOT NULL,
  `comment` text,
  `subscriber_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_interview_subscriber1_idx` (`subscriber_id`),
  CONSTRAINT `fk_interview_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interview`
--

LOCK TABLES `interview` WRITE;
/*!40000 ALTER TABLE `interview` DISABLE KEYS */;
INSERT INTO `interview` VALUES (4,'behavioral','passed','Advanced technical knowledge. \r\nEnglish level is B2.\r\nPrefers to work in Minsk.',1),(5,'group','appointed',' ',3),(10,'behavioral','appointed','test interview',37),(11,'introductory','passed','test asp',28),(13,'technical','appointed','Any information about interview. Any information about interview.\r\n\r\nAny information about interview.',28),(14,'introductory','passed','test',31),(15,'introductory','passed','sgdsgsdg',31),(17,'introductory','passed','23',31),(20,'introductory','passed','Tested note',36);
/*!40000 ALTER TABLE `interview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` enum('admin','hr','aspirant') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'aspirant'),(2,'hr'),(3,'admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriber`
--

DROP TABLE IF EXISTS `subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_active` bit(1) DEFAULT b'1',
  `client_id` int(10) unsigned NOT NULL,
  `vacancy_id` int(10) unsigned NOT NULL,
  `status` enum('submitted','interview_appointed','job_offer','rejected') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_job_vacancy_has_user_job_vacancy1_idx` (`vacancy_id`),
  KEY `fk_subscriber_client1_idx` (`client_id`),
  CONSTRAINT `fk_job_vacancy_has_user_job_vacancy1` FOREIGN KEY (`vacancy_id`) REFERENCES `vacancy` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriber`
--

LOCK TABLES `subscriber` WRITE;
/*!40000 ALTER TABLE `subscriber` DISABLE KEYS */;
INSERT INTO `subscriber` VALUES (1,'\0',1,1,'submitted'),(3,'\0',4,9,'job_offer'),(24,'\0',1,2,'rejected'),(25,'\0',1,8,'rejected'),(26,'\0',1,6,'submitted'),(27,'\0',1,9,'rejected'),(28,'',1,4,'submitted'),(29,'',1,3,'submitted'),(30,'\0',1,5,'job_offer'),(31,'',4,1,'submitted'),(32,'\0',4,2,'submitted'),(33,'',4,3,'interview_appointed'),(34,'',1,7,'interview_appointed'),(36,'',4,5,'interview_appointed'),(37,'',4,6,'job_offer'),(38,'',1,10,'submitted'),(39,'',6,4,'submitted'),(40,'\0',1,18,'submitted'),(41,'',1,21,'job_offer');
/*!40000 ALTER TABLE `subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy`
--

DROP TABLE IF EXISTS `vacancy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `specialization` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `description` text,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy`
--

LOCK TABLES `vacancy` WRITE;
/*!40000 ALTER TABLE `vacancy` DISABLE KEYS */;
INSERT INTO `vacancy` VALUES (1,'.NET Developer','MINSK, BELARUS','Currently we are looking for a .NET Developer for our Minsk office to make the team even stronger.\r\nWe build desktop applications as well as web applications for ships’ routing optimization and weather data analysis. And at the same time we enhance functionality of the existing applications related to weather data.\r\nJoining us, you will have a great chance to level up your communication skills, get incredible experience working in a distributed team (with the Dutch and Germans) as well as enjoy the possibility of business trips to the Netherlands and outstanding career development opportunities.','\0'),(2,'Android Developer','MINSK, BELARUS','Currently we are looking for an Android Developer for our Minsk office to make the team even stronger.\r\nAs an Android Developer, you will be building native mobile applications for Fortune 500 companies. You will be working in close collaboration with the client, as well as other teams across EPAM paying special attention to solutions’ architecture and code quality.',''),(3,'Application Security Engineer/Architect','BREST, BELARUS','Currently we are looking for an Application Security Engineer/Architect for our Minsk office to make the team even stronger.\r\nAs an Application Security Engineer, you will be responsible for increasing Security Awareness among Project Teams and driving security activities through the entire SDLC.\r\nThe candidate is supposed to work at our Brest office, but BY regions can be considered as well and relocation to Minsk is supported.',''),(4,'C++ Developer','MINSK, BELARUS','Currently we are looking for a great C++ Developer to reinforce our team in Minsk.\r\nWhat’s it all about? – Our account is working with a publishing company known across the globe and has offices in 40+ countries. We are engaged in creation and development of the platforms, which keep and deliver healthcare content (thematic and professional). The platforms are used both by US medical university students and by doctors during real surgeries. We make contribution into saving lives.\r\nOur big and growing team (whole program is 70+ engineers) is working with the heart of this platform - the search engine, based on the C++. One of our challenges here is to help people to find accurate information in the shortest period of time and to keep them up-to-date in their professional life.',''),(5,'Desktop Test Automation Engineer (Mac OS)','MINSK, BELARUS','Currently we are looking for a Desktop Test Automation Engineer (Mac OS) for our Minsk office to make our team even stronger.\r\nAs a Desktop Test Automation Engineer, you will be working on a very interesting project for a Silicon Valley customer, the leader in enterprise secure file sharing and synchronization. \r\nComplex code, heavy computing, cutting edge technologies – it’s all about this project. Together with the customer team, we develop brand new functionality, which does not exist in the market at the current moment.',''),(6,'DevOps Architect','MINSK, BELARUS','Currently we are looking for a DevOps Architect to make the team even stronger.\r\nYou will be working as an innovator and thought leader in the infrastructure, build and delivery space. You will work with teams including Release Management, Operations, Production Engineering, Test and Development to ensure end-to-end solutions are designed, implement to simplify, and to make sure the build and delivery mechanisms are reliable, transparent, scalable, and transportable. We are looking for someone who will bring their Release Engineering and infrastructure expertise, creativity, passion, and innovative experience and who is ready to proactively function on a highly visible team in a fast-paced environment.\r\n',''),(7,'Development Team Leader','TORONTO, CANADA','Currently we are looking for a Development Team Leader with excellent communication skills for our Toronto office to make the team even stronger.\r\nAs a Team Leader, you will coordinate and drive the building of the product that will serve our client’s customers. The solution we develop is an innovative and robust cloud-based human capital management platform, which provides a full-scale real-time toolset for managing all aspects of employee’s lifecycle from pre-hiring to retirement. ',''),(8,'Drupal Solution Architect','MINSK, BELARUS','We are currently looking for a Drupal Solution Architect for our Minsk office to make our team even stronger.\r\nAs a Drupal Solution Architect, you will be leading the design and implementation of web based solutions. These frequently contain complex Content Management implementations. Additionally, you will lead the design of business object models, application business objects, business services, implementation framework and application services. You will be capturing the requirements for these services and identifying the techniques, design and implementation patterns, and technologies that should be applied at the architecture and application level. You are also responsible for helping to define the system and application architecture, functional and non-functional requirements and software services that the architecture, application and computing infrastructure support.',''),(9,'Account Director','NEW YORK, NY, USA','You are strategic, resilient, engaging with people and a natural self-starter. You have a passion for solving complex problems. If this sounds like you, this could be the perfect opportunity to join EPAM as an Account Director. Scroll down to learn more about the position’s responsibilities and requirements.\r\nOur ideal candidate will act as the point of contact for clients, prospects, EPAM Account Managers, Business Unit Heads and Sales Leaders. You will be creating business strategies, architect and oversee delivery approaches, build successful relationships, and drive business results.',''),(10,'Back-end API Engineer ','NEW YORK, NY, USA','You are curious, persistent, logical and clever – a true techie at heart. You enjoy living by the code of your craft and developing elegant solutions for complex problems. If this sounds like you, this could be the perfect opportunity to join EPAM as a Back-end API Engineer. Scroll down to learn more about the position’s responsibilities and requirements.\r\nWe’re building exciting, meaningful products that push forward the intersection of emerging technologies and our incredibly influential lifestyle brand and content. \r\nAs part of our team, you will influence how and why we build products, as well as the practices and patterns we use across our development organization. You’ll play a critical ownership role in validating new ideas and concepts, building new features, enabling data-driven collaboration, and exploring new ways that the mobile experience can change our customers’ lives for the better.',''),(11,'C# Developer','GRODNO, BELARUS','Currently we are looking for a C# Developer for our Grodno office (to work remotely from Volgograd or with relocation) to make the team even stronger.\r\n\r\nAs a C# Developer, you will have a chance to work on an interesting project with a modern technology stack. Among our customers are the biggest companies in their branches. We offer you an exciting opportunity to join our close-knit team of professionals and climb the ladder of success in the leading IT company.',''),(13,'Account Director, Media & Entertainment','NEW YORK, NY, USA','You are strategic, resilient, engaging with people and a natural self-starter. You have a passion for solving complex problems. If this sounds like you, this could be the perfect opportunity to join our compony as an Account Director, Media & Entertainment . Scroll down to learn more about the position’s responsibilities and requirements.\r\nRESPONSIBILITIES:\r\nAct as a subject matter expert within Account Management for media and entertainment related projects/clients.\r\nCreate business strategies to successfully achieve client business goals\r\nAct as the Account lead with clients, internal teams and BU heads.',''),(14,'.NET Developer','Zurich, Switzerland','DESCRIPTION:\r\nCurrently we are looking for a .NET Developer for our Zurich office to make the team even stronger.\r\n\r\nRESPONSIBILITIES:\r\nPresentation of service layer .NET development for Exchange 2016 integration\r\n\r\nREQUIREMENTS:\r\nMicrosoft Exchange expert level knowledge is key\r\nMicrosoft Exchange Web Service knowledge\r\nMicrosoft Exchange configuration (e.g. impersonation)\r\nAbility to manage a diverse workload effectively in a highly demanding environment\r\nHigh level of English\r\nKnowledge of German is an advantage\r\nHighly motivated and capable of both working independently and in a team',''),(15,'.NET Full Stack Developer','Minsk, Belarus','.NET community! Currently we are looking for a .NET Full Stack Developer to reinforce our team in order to join a project stream for our customer dealing with Intellectual property domain.\r\n\r\nTogether we develop a set of applications which allow to search/view/create/update information related to intellectual property, payments for it etc. These applications are used by other companies and their attorneys, docketers, etc. to track such information.\r\n\r\nWe are going to develop a system from scratch together with existing subsystems upgrading them to a modern level. Our project goals assume to use modern front-end experience to create progressive UI with best approaches in back-end development.',''),(16,'.NET Full Stack Software Engineer','Kyiv, Ukraine','Our client is a global company that provides information, software, and services. Their customers are legal, business, tax, accounting, finance, audit, risk, compliance, and healthcare professionals. \r\n\r\nOn this position you will be working on building a market leading solution to institutions and regulators which automates collection, verification and certification of compliance data. In addition, solution allows creating custom reports and do a real-time auditing to automate compliance and reduce risks. The team is going to be fully cross-functional including developers, functional/automation testers and business analysts. Developing of next gen report, replatforming legacy set of application to unified SaaS-based regulatory compliance solution, ASP NET MVC/Web API.',''),(17,'.NET Solution Architect','London, UK','Currently we are looking for a permanent .NET Solution Architect to join our European Architecture Practice and lead on strategic client projects.\r\n\r\nThe initial project will be in Nottingham (potentially 1-2 years) and then later aligned to different UK clients. We therefore need someone that is UK travel ready once the intial project comes to an end.\r\n\r\nJoin us at one of the fastest growing Software Engineering Organizations in the UK!\r\nWe are seeking a Solution Architect who is responsible for one or more sub-domains of an enterprise system and work out the design. You will need to follow the policies, patterns and ensure that these are followed in the product as well. Moreover, you will need to work with one or more teams from different countries and as the role grows, you will need to travel to development teams sites in different locations.',''),(18,'.NET/AngularJS Developer','Prague, Czech Republic','DESCRIPTION\r\nCurrently we are looking for a .NET/AngularJS Developer for our Prague office to make the team even stronger.\r\n\r\nABOUT PROJECT\r\nOur client is an investment bank which provides large corporate, government and institutional clients with a full spectrum of solutions to their strategic advisory, financing and risk management needs. The successful candidate will join a team responsible for the development of a plugin-based web application using C#, ASP.NET MVC, JavaScript (Angular, React) and HTML5/CSS3. The successful candidate will be a key developer within this Agile team, contributing to all parts of the development process and offering strong technical designs and enhancements.',''),(19,'AEM Developer','Shenzhen, China','DESCRIPTION:\r\nCurrently we are looking for an AEM Developer Shenzhen Delivery Center to make the team even stronger.\r\nRESPONSIBILITIES:\r\nWork closely with client to design and implement solutions for digital consumer experiences based on the foundational Adobe EM product suite\r\nTranslate business requirements to components and services design using AEM functionality\r\nResolve technical problems; critical thinking on development approach\r\nDiagnose and solve technical problems related to content management implementation\r\nAdopt and utilize iterative/Agile methodology as needed or requested',''),(20,'AEM Lead Developer','Toronto, Canada','DESCRIPTION:\r\nYou are curious, persistent, logical and clever – a true techie at heart. You enjoy living by the code of your craft and developing elegant solutions for complex problems. If this sounds like you, this could be the perfect opportunity to join EPAM as a AEM Lead Developer. Scroll down to learn more about the position’s responsibilities and requirements.\r\nRESPONSIBILITIES:\r\nPlay a vital role in the development, design, testing and deployment of Adobe Experience Management Web and Mobile solutions.\r\nWork closely with clients, developers, system administrators, project managers, business analysts and end users to build a state-of-the-art .Digital Platform using Adobe\'s Digital Marketing Product Suite.\r\nDesign efficient content models, security models, workflows and templates.\r\nDevelop AEM templates and components leveraging current AEM software releases.\r\nEmploy strong coding standards for efficiency, readability, and reuse.',''),(21,'Account Manager','Amsterdam, Netherlands','DESCRIPTION\r\nCurrently we are looking for an Account Manager for our Amsterdam office to make the team even stronger.\r\n\r\nOur compony, the winner of the Most Transformative Branch Experience award in the Citi Mobile Challenge, is looking to add an Account Manager to join our team of professionals. Our ideal candidate will act as the point of contact for clients, prospects, Account Managers, Business Unit Heads and Sales Leaders. The Account Manager will create business strategies, architect and oversee delivery approaches, build successful relationships, and drive business results.',''),(22,'Account Manager','New York, NY, USA','DESCRIPTION:\r\nYou are strategic, resilient, engaging with people and a natural self-starter. You have a passion for solving complex problems. If this sounds like you, this could be the perfect opportunity to join our comand as an Account Manager. Scroll down to learn more about the position’s responsibilities and requirements.\r\n\r\nOur ideal candidate will act as the point of contact for clients, prospects, Account Managers, Business Unit Heads and Sales Leaders. The Account Manager will create business strategies, architect and oversee delivery approaches, build successful relationships, and drive business results.',''),(23,'Accounts Receivable Specialist','Minsk, Belarus','DESCRIPTION:\r\nCurrently we are looking for an Accounts Receivable Specialist for our Minsk office to make our team even stronger.\r\nRESPONSIBILITIES:\r\nGenerate and send out invoices.\r\nPerform day-to-day financial transactions, including verifying, posting and recording accounts receivables’ data.\r\nCommunicate with customers and business order to cash related questions (especially accounts receivable collection).\r\nOperational work with contracts.\r\nAudit support in terms of providing additional requested information.\r\nAd-hock requests related to order to cash business cycle.',''),(24,'Android Developer','Lviv, Ukraine','DESCRIPTION\r\n\r\nWith over 2 million websites using our web player, we know a thing or two about making video easy for developers to work with. Our mobile SDKs aim to provide those same world-class video tools to native app developers building Android and iOS applications. We’re not looking to make the next cool app -- we’re looking to enable the next thousand cool apps (and beyond) to build engaging video experiences.\r\n\r\nOur Mobile team is looking for an accomplished software engineer with commercial experience creating and shipping native Android applications. You will use your skills to enhance our JW Player SDK for Android, which enables developers to easily build video and audio playback functionality into any Android application.','');
/*!40000 ALTER TABLE `vacancy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-16 21:44:18
