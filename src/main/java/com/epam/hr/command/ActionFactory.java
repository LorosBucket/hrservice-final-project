package com.epam.hr.command;

import com.epam.hr.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {

    /**
     * Returns a command from the command enum
     * corresponding to the parameter 'command'
     * from the request.
     *
     * @param request HttpServletRequest
     * @return ActionCommand
     * @throws CommandException if IllegalArgumentException
     *                          has been thrown.
     */
    public ActionCommand defineCommand(HttpServletRequest request) throws CommandException {
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            throw new CommandException("Command is null.");
        }
        try {
            String actionUpperCase = action.toUpperCase();
            CommandEnum commandEnum = CommandEnum.valueOf(actionUpperCase);
            return commandEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
