package com.epam.hr.command;

import com.epam.hr.command.impl.*;

/**
 * All application commands.
 *
 * @author Dolnikov Vladislav
 */
public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    SHOW_VACANCIES {
        {
            this.command = new ShowVacanciesCommand();
        }
    },
    SHOW_ASPIRANT_VACANCIES {
        {
            this.command = new ShowAspirantVacanciesCommand();
        }
    },
    ADD_SUBSCRIBER{
        {
            this.command = new AddSubscriberCommand();
        }
    },
    HIDE_SUBSCRIBER{
        {
            this.command = new HideSubscriberCommand();
        }
    },
    SHOW_ASPIRANTS_LIST{
        {
            this.command = new ShowAspirantsListCommand();
        }
    },
    SHOW_HRS_LIST{
        {
            this.command = new ShowHrsListCommand();
        }
    },
    BLOCK_ACCOUNT{
        {
            this.command = new BlockAccountCommand();
        }
    },
    UNBLOCK_ACCOUNT{
        {
            this.command = new UnblockAccountCommand();
        }
    },
    HIDE_VACANCY{
        {
            this.command = new HideVacancyCommand();
        }
    },
    SHOW_VACANCIES_WITH_RESPONSES {
        {
            this.command = new ShowVacanciesWithResponsesCommand();
        }
    },
    UPDATE_SUBSCRIBER_STATUS {
        {
            this.command = new UpdateSubscriberStatusCommand();
        }
    },
    SHOW_VACANCY_SUBSCRIBERS{
        {
            this.command = new ShowVacancySubscribersCommand();
        }
    },
    ADD_VACANCY {
        {
            this.command = new AddVacancyCommand();
        }
    },
    SUBSCRIBER_CONTROL {
        {
            this.command = new SubscriberControlCommand();
        }
    },
    DELETE_INTERVIEW {
        {
            this.command = new DeleteInterviewCommand();
        }
    },
    ADD_INTERVIEW {
        {
            this.command = new AddInterviewCommand();
        }
    },
    EDIT_VACANCY {
        {
            this.command = new EditVacancyCommand();
        }
    },
    CHANGE_LOCAL {
        {
            this.command = new ChangeLocalCommand();
        }
    },
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    };
    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
