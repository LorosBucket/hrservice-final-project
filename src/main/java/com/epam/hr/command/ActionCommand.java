package com.epam.hr.command;

import com.epam.hr.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Applications command interface.
 */
public interface ActionCommand {
    String CONTROLLER_COMMAND = "/controller?command=";

    void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
