package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.dao.dto.SubscriptionDto;
import com.epam.hr.entity.user.User;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.SubscriberService;
import com.epam.hr.service.impl.SubscriberServiceImpl;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Command for aspirant vacancies showing.
 *
 * @author Dolnikov Vladislav
 */
public class ShowAspirantVacanciesCommand implements ActionCommand {

    /**
     * Finds aspirant vacancies by ID using subscriber service and
     * redirects to the relevant page.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException, ServletException
     *                          or IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Integer userId = user.getEntityId();

        try {
            SubscriberService subscriberService = new SubscriberServiceImpl();
            List<SubscriptionDto> subscriptionsDtoList = subscriberService.getAspirantSubscriptions(userId);

            request.setAttribute("subscriptionsDtoList", subscriptionsDtoList);
            request.setAttribute("dtoListSize", subscriptionsDtoList.size());
            PageDispatcher.doForward(request, response, Pages.ASPIRANT_SUBSCRIPTIONS.getUrl());
        } catch (ServiceException | ServletException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
