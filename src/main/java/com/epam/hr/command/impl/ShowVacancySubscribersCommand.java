package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.dao.dto.SubscriptionDto;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.SubscriberService;
import com.epam.hr.service.impl.SubscriberServiceImpl;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Command for vacancy subscribers showing.
 *
 * @author Dolnikov Vladislav
 */
public class ShowVacancySubscribersCommand implements ActionCommand {

    /**
     * Gets parameter vacancyId from a request and saves attribute
     * Dto list of subscriptions. Redirects to the vacancy_subscribers.jsp.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        try {
            SubscriberService subscriberService = new SubscriberServiceImpl();

            Integer vacancyId = getVacancyId(request);
            List<SubscriptionDto> subscriptionsDtoList = subscriberService.getVacancySubscribers(vacancyId);
            request.setAttribute("subscriptionsDtoList", subscriptionsDtoList);

            PageDispatcher.doForward(request, response, Pages.VACANCY_SUBSCRIBERS.getUrl());
        } catch (ServiceException | ServletException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private Integer getVacancyId(HttpServletRequest request) {
        String vacancyIdStr = request.getParameter("vacancyId");
        return Integer.parseInt(vacancyIdStr);
    }
}
