package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.command.CommandEnum;
import com.epam.hr.entity.vacancy.Vacancy;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.VacancyService;
import com.epam.hr.service.impl.VacancyServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for vacancy editing.
 *
 * @author Dolnikov Vladislav
 */
public class EditVacancyCommand implements ActionCommand {
    private static final String MODAL_FRAME_ID = "#modal_frame";

    /**
     * Gets parameter vacancy ID from the request,
     * finds vacancy with this ID in a database and
     * saves it in the session to display and edit later.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException or
     *                          IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        Integer vacancyId = getVacancyId(request);

        try {
            VacancyService vacancyService = new VacancyServiceImpl();
            Vacancy vacancyToEdit = vacancyService.getVacancyById(vacancyId);

            request.getSession().setAttribute("editedVacancy", vacancyToEdit);
            String page = request.getContextPath() + CONTROLLER_COMMAND
                    + CommandEnum.SHOW_VACANCIES + MODAL_FRAME_ID;
            response.sendRedirect(page);
        } catch (ServiceException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private Integer getVacancyId(HttpServletRequest request) throws CommandException {
        String vacancyIdStr = request.getParameter("vacancyId");
        if (vacancyIdStr == null) {
            throw new CommandException("Vacancy id is null.");
        }
        return Integer.parseInt(vacancyIdStr);
    }
}
