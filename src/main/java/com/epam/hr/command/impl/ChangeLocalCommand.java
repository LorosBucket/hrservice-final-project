package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Command for changing local.
 *
 * @author Dolnikov Vladislav
 */
public class ChangeLocalCommand implements ActionCommand {

    private static final Logger LOGGER = LogManager.getLogger(ChangeLocalCommand.class);

    private static final String LITERAL_QUESTION = "?";
    private static final String REFERER = "Referer";
    private static final String ROOT_PATH = "./";
    private static final String PAGE = "page";
    private static final String LOCAL = "local";
    private static final String CONTROLLER = "controller";

    /**
     * Get url and parameters of the request, redirect to current page.
     *
     * @param request  httpServletRequest
     * @param response httpServletResponse
     * @throws CommandException if IOExceptions has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        HttpSession session = request.getSession();
        String targetLocal = request.getParameter(LOCAL);
        session.setAttribute(LOCAL, targetLocal);
        LOGGER.debug("Local '" + targetLocal + "' has been installed");

        StringBuilder url = new StringBuilder(ROOT_PATH);

        String referer = request.getHeader(REFERER);

        if (referer != null && referer.lastIndexOf(LITERAL_QUESTION) > 0) {
            url.append(CONTROLLER);
            String query = referer.substring(referer.lastIndexOf(LITERAL_QUESTION));
            url.append(query);
        } else {
            String currentPage = (String) session.getAttribute(PAGE);
            url.append(currentPage);
        }

        try {
            response.sendRedirect(url.toString());
        } catch (IOException e) {
            throw new CommandException(e.getMessage(), e);
        }

    }
}
