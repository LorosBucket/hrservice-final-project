package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.entity.interview.Interview;
import com.epam.hr.entity.subscriber.SubscriberStatus;
import com.epam.hr.entity.user.User;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.InterviewService;
import com.epam.hr.service.SubscriberService;
import com.epam.hr.service.UserService;
import com.epam.hr.service.impl.InterviewServiceImpl;
import com.epam.hr.service.impl.SubscriberServiceImpl;
import com.epam.hr.service.impl.UserServiceImpl;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Command for subscriber control page loading.
 *
 * @author Dolnikov Vladislav
 */
public class SubscriberControlCommand implements ActionCommand {
    private static final String SUBSCRIBER_ID = "subscriberId";
    private static final String USER_ID = "userId";

    /**
     * Gets interviews list and subscriber status from
     * a database, sets parameters in the request and
     * redirects to the relevant page.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException,
     *                          ServletException or IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        try {
            Integer subscriberId = getIntegerParameter(request, SUBSCRIBER_ID);
            Integer userId = getIntegerParameter(request, USER_ID);

            setAttrInterviewsList(request, subscriberId);
            setAttrSubscriberStatus(request, subscriberId);
            setAttrUser(request, userId);

            request.getSession().setAttribute(USER_ID, userId);
            request.getSession().setAttribute(SUBSCRIBER_ID, subscriberId);

            PageDispatcher.doForward(request, response, Pages.SUBSCRIBER_CONTROL.getUrl());
        } catch (ServiceException | ServletException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private Integer getIntegerParameter(HttpServletRequest request, String param) {
        String paramStr = request.getParameter(param);
        if (paramStr != null) {
            return Integer.parseInt(paramStr);
        }
        return (Integer) request.getSession().getAttribute(param);
    }

    private void setAttrUser(HttpServletRequest request, Integer userId) throws ServiceException {
        UserService userService = new UserServiceImpl();
        User user = userService.getById(userId);
        request.setAttribute("user", user);
    }

    private void setAttrInterviewsList(HttpServletRequest request, Integer subscriberId) throws ServiceException {
        InterviewService interviewService = new InterviewServiceImpl();
        List<Interview> interviewsList = interviewService.getInterviews(subscriberId);
        request.setAttribute("interviewsList", interviewsList);
    }

    private void setAttrSubscriberStatus(HttpServletRequest request, Integer subscriberId) throws ServiceException {
        SubscriberService subscriberService = new SubscriberServiceImpl();

        SubscriberStatus subscriberStatus = subscriberService.getSubscriberStatus(subscriberId);
        String result = subscriberStatus.toString();
        request.setAttribute("subscriber_status", result);
    }
}
