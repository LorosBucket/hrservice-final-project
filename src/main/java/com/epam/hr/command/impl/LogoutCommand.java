package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.exception.CommandException;
import com.epam.hr.util.Pages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Command for system signing out.
 *
 * @author Dolnikov Vladislav
 */
public class LogoutCommand implements ActionCommand {
    private static final Logger LOGGER = LogManager.getLogger(LogoutCommand.class);

    /**
     * Invalidates user's session and redirects to the index page.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        HttpSession session = request.getSession();

        session.invalidate();
        LOGGER.debug("Session has been invalidated.");

        String page = request.getContextPath() + Pages.INDEX.getUrl();
        try {
            response.sendRedirect(page);
        } catch (IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
