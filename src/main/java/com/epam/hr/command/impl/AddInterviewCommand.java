package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.command.CommandEnum;
import com.epam.hr.entity.interview.InterviewStatus;
import com.epam.hr.entity.interview.InterviewType;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.InterviewService;
import com.epam.hr.service.impl.InterviewServiceImpl;
import com.epam.hr.util.EncoderUtil;
import com.epam.hr.util.validator.ValidatorUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for an interview saving in a database.
 * Validates input parameters for NULL and empty values
 * after client validation. Throws CommandException if invalid.
 *
 * @author Dolnikov Vladislav
 */
public class AddInterviewCommand implements ActionCommand {

    /**
     * Gets parameters from the request and session,
     * validates and calls method save of InterviewService.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if invalid input parameters,
     *                          if IOException or ServiceException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String typeStr = request.getParameter("type");
        String statusStr = request.getParameter("status");
        String comment = request.getParameter("comment");
        comment = EncoderUtil.encodeToUtf8(comment);

        Integer subscriberId = (Integer) request.getSession().getAttribute("subscriberId");

        boolean notNull = ValidatorUtils.checkParamsForNull(typeStr, statusStr, comment, subscriberId);
        boolean notEmpty = ValidatorUtils.validateOnEmptyString(typeStr, statusStr, comment);
        if (!notEmpty || !notNull) {
            throw new CommandException("Illegal interview input data.");
        }

        String typeUpperCase = typeStr.toUpperCase();
        InterviewType type = InterviewType.valueOf(typeUpperCase);

        String statusUpperCase = statusStr.toUpperCase();
        InterviewStatus status = InterviewStatus.valueOf(statusUpperCase);

        try {
            InterviewService interviewService = new InterviewServiceImpl();
            interviewService.save(subscriberId, type, status, comment);

            redirect(request, response);
        } catch (ServiceException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private void redirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String page = request.getContextPath() + CONTROLLER_COMMAND + CommandEnum.SUBSCRIBER_CONTROL;
        response.sendRedirect(page);
    }

}
