package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.command.CommandEnum;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.VacancyService;
import com.epam.hr.service.impl.VacancyServiceImpl;
import com.epam.hr.util.EncoderUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for new vacancy adding.
 *
 * @author Dolnikov Vladislav
 */
public class AddVacancyCommand implements ActionCommand {

    /**
     * Gets parameters: specialization, location,
     * description, status and vacancy ID from request.
     * Saves vacancy using vacancy service.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException or
     *                          IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String specialization = request.getParameter("specialization");
        String location = request.getParameter("location");
        String description = request.getParameter("description");
        description = EncoderUtil.encodeToUtf8(description);
        String statusStr = request.getParameter("status");
        String vacancyIdStr = request.getParameter("vacancyId");

        try {
            if (statusStr == null) {
                throw new CommandException("Vacancy status is null.");
            }
            byte status = Byte.parseByte(statusStr);

            Integer vacancyId = null;
            if (!vacancyIdStr.isEmpty()) {
                vacancyId = Integer.parseInt(vacancyIdStr);
            }

            VacancyService vacancyService = new VacancyServiceImpl();
            vacancyService.save(vacancyId, specialization, location, description, status);

            redirect(request, response);
        } catch (ServiceException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private void redirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String page = request.getContextPath() + CONTROLLER_COMMAND + CommandEnum.SHOW_VACANCIES;
        response.sendRedirect(page);
    }

}
