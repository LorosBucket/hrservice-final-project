package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.command.CommandEnum;
import com.epam.hr.entity.user.User;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.SubscriberService;
import com.epam.hr.service.impl.SubscriberServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Command for vacancy response canceling.
 *
 * @author Dolnikov Valadislav
 */
public class HideSubscriberCommand implements ActionCommand {

    /**
     * Gets parameters user ID and vacancy ID from the request
     * and hide subscriber using subscriber service.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException or
     *                          IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        Integer userId = user.getEntityId();
        String vacancyIdStr = request.getParameter("vacancyId");
        Integer vacancyId = Integer.parseInt(vacancyIdStr);

        try {
            SubscriberService subscriberService = new SubscriberServiceImpl();
            subscriberService.hideSubscriber(userId, vacancyId);

            String page = request.getContextPath() + CONTROLLER_COMMAND + CommandEnum.SHOW_ASPIRANT_VACANCIES;
            response.sendRedirect(page);
        } catch (ServiceException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
