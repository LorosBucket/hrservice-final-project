package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.command.CommandEnum;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.InterviewService;
import com.epam.hr.service.impl.InterviewServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for interview hiding.
 *
 * @author Dolnikov Vladislav
 */
public class DeleteInterviewCommand implements ActionCommand {

    /**
     * Gets parameter interview ID from the request
     * and hide interview using interview service.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException
     *                          or IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String interviewIdStr = request.getParameter("interviewId");

        if (interviewIdStr == null) {
            throw new CommandException("Interview id is null.");
        }

        Integer interviewId = Integer.parseInt(interviewIdStr);
        try {
            InterviewService interviewService = new InterviewServiceImpl();
            interviewService.deleteById(interviewId);

            String page = request.getContextPath() + CONTROLLER_COMMAND + CommandEnum.SUBSCRIBER_CONTROL;
            response.sendRedirect(page);
        } catch (ServiceException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
