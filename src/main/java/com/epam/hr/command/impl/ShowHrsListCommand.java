package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.UserService;
import com.epam.hr.service.impl.UserServiceImpl;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Command for hr-specialists showing.
 *
 * @author Dolnikov Vladislav
 */
public class ShowHrsListCommand implements ActionCommand {

    /**
     * Finds hr-specialist in a database and displays them.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException,
     *                          ServletException or IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        try {
            UserService userService = new UserServiceImpl();
            List<User> hrsList = userService.getUsersByRole(UserRole.HR);
            request.setAttribute("hrsList", hrsList);

            PageDispatcher.doForward(request, response, Pages.HRS_LIST.getUrl());
        } catch (ServiceException | ServletException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
