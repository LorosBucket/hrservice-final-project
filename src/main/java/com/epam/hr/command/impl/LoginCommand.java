package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.entity.user.User;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.UserService;
import com.epam.hr.service.impl.UserServiceImpl;
import com.epam.hr.util.EncoderUtil;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;
import com.epam.hr.util.validator.LoginValidator;
import com.epam.hr.util.validator.PasswordValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Command for system signing in.
 *
 * @author Dolnikov Vladislav
 */
public class LoginCommand implements ActionCommand {
    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);

    private static final String PASSWORD = "password";
    private static final String LOCAL_ACCOUNT_SUSPENDED = "local.account.suspended";
    private static final String LOCAL_LOGIN_ERROR = "local.login.error";
    private static final String LOCAL = "local";
    private static final String USER_LANGUAGE = "user.language";
    private static final String LOGIN = "login";
    private static final String USER = "user";
    private static final String ERROR_LOGIN_PASS_MESSAGE = "errorLoginPassMessage";

    /**
     * Gets parameters: login and password from the request,
     * validates and redirects according result.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException or
     *                          IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String login = request.getParameter(LOGIN);
        login = EncoderUtil.encodeToUtf8(login);
        String password = request.getParameter(PASSWORD);

        if (!LoginValidator.validate(login) || !PasswordValidator.validate(password)) {
            processIllegalInput(request, response);
            return;
        }

        UserService userService = new UserServiceImpl();

        try {
            User user = userService.authorizeUser(login, password);
            if (user == null) {
                processInvalidLoginOrPassword(request, response);
                return;
            }

            boolean isBlocked = user.getIsBlocked();
            if (isBlocked) {
                processBlockedAccountInput(request, response);
            } else {
                putUserInSession(request, user);
                deleteMessages(request);
                PageDispatcher.redirectToHomePage(request, response, user.getRole());
            }
        } catch (ServiceException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private void deleteMessages(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(ERROR_LOGIN_PASS_MESSAGE);
    }

    private void processBlockedAccountInput(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        /*User's account has been suspended*/
        LOGGER.info("User's account has been suspended");
        HttpSession session = request.getSession();
        session.setAttribute(ERROR_LOGIN_PASS_MESSAGE, LOCAL_ACCOUNT_SUSPENDED);
        redirectToLogin(request, response);
    }

    private void processInvalidLoginOrPassword(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        /*Nonexistent pair login-password*/
        LOGGER.info("Nonexistent pair login-password");
        HttpSession session = request.getSession();
        session.setAttribute(ERROR_LOGIN_PASS_MESSAGE, LOCAL_LOGIN_ERROR);
        redirectToLogin(request, response);
    }

    private void processIllegalInput(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        /* Illegal login or(and) password has been entered */
        LOGGER.info("Illegal login or(and) password has been entered ");
        HttpSession session = request.getSession();
        session.setAttribute(ERROR_LOGIN_PASS_MESSAGE, LOCAL_LOGIN_ERROR);
        redirectToLogin(request, response);
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = request.getContextPath() + Pages.LOGIN.getUrl();
        try {
            response.sendRedirect(page);
        } catch (IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private void putUserInSession(HttpServletRequest request, User user) {
        HttpSession session = request.getSession();
        String login = user.getLogin();
        session.setAttribute(LOGIN, login);
        session.setAttribute(USER, user);

        if (session.getAttribute(LOCAL) == null) {
            String lang = System.getProperty(USER_LANGUAGE);
            session.setAttribute(LOCAL, lang);
        }
    }
}
