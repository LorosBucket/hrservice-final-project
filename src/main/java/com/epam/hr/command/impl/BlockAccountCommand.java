package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.UserService;
import com.epam.hr.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for user account suspending.
 *
 * @author Dolnikov Vladislav
 */
public class BlockAccountCommand implements ActionCommand {

    /**
     * Get parameter user ID and suspend user account
     * using user service.
     * Redirects to the commandPage from request after
     * action.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException or
     *                          IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        try {
            String userIdStr = request.getParameter("userId");
            String page = request.getParameter("commandPage");

            if (userIdStr == null) {
                throw new CommandException("UserId is null.");
            }
            Integer userId = Integer.parseInt(userIdStr);

            UserService userService = new UserServiceImpl();
            userService.setAccountBlocking(userId, true);

            response.sendRedirect(page);
        } catch (ServiceException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
