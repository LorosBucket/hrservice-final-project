package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.entity.vacancy.Vacancy;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.VacancyService;
import com.epam.hr.service.impl.VacancyServiceImpl;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Command for vacancies showing.
 *
 * @author Dolnikov Vladislav
 */
public class ShowVacanciesCommand implements ActionCommand {
    private static final int DEFAULT_PAGE_INDEX = 1;
    private static final int DEFAULT_COUNT = 7;
    private static final String PAGE_INDEX = "pageIndex";
    private static final String COUNT = "count";

    /**
     * Finds vacancies in a database using vacancy service
     * and displays them.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException,
     *                          ServletException or IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        try {

            Integer pageIndex = getParam(request, PAGE_INDEX, DEFAULT_PAGE_INDEX);
            Integer count = getParam(request, COUNT, DEFAULT_COUNT);

            VacancyService vacancyService = new VacancyServiceImpl();
            Integer vacanciesAmount = vacancyService.getVacanciesAmount();

            request.setAttribute("vacanciesAmount", vacanciesAmount);

            List<Vacancy> vacanciesList = vacancyService.getActiveVacancies(pageIndex, count);
            request.setAttribute("vacanciesList", vacanciesList);

            PageDispatcher.doForward(request, response, Pages.VACANCIES_LIST.getUrl());
        } catch (ServiceException | ServletException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private Integer getParam(HttpServletRequest request, String paramName, int defaultParam) {
        String paramStr = request.getParameter(paramName);

        Integer param = defaultParam;
        if (paramStr != null && !paramStr.isEmpty()) {
            param = Integer.parseInt(paramStr);
        }
        return param;
    }
}
