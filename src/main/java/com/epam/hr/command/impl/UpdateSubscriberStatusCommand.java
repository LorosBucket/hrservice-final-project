package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.command.CommandEnum;
import com.epam.hr.entity.subscriber.SubscriberStatus;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.SubscriberService;
import com.epam.hr.service.impl.SubscriberServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for subscriber status changing.
 *
 * @author Dolnikov Vladislav
 */
public class UpdateSubscriberStatusCommand implements ActionCommand {

    /**
     * Gets subscriber id and status from the request and
     * update values in a database using subscriber service.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException,
     *                          ServletException or IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        try {
            String subscriberIdStr = request.getParameter("subscriberId");
            String statusStr = request.getParameter("status");

            if (subscriberIdStr == null || statusStr == null) {
                throw new CommandException("subscriberId or status is null.");
            }

            Integer subscriberId = Integer.parseInt(subscriberIdStr);
            String statusUpperCase = statusStr.toUpperCase();
            SubscriberStatus status = SubscriberStatus.valueOf(statusUpperCase);

            SubscriberService subscriberService = new SubscriberServiceImpl();
            subscriberService.updateSubscriberStatus(subscriberId, status);

            String page = request.getContextPath() + CONTROLLER_COMMAND + CommandEnum.SUBSCRIBER_CONTROL;
            response.sendRedirect(page);
        } catch (ServiceException | IOException e) {
            throw new CommandException("Subscriber status changing error." + e.getMessage(), e);
        }
    }
}
