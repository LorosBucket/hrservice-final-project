package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.UserService;
import com.epam.hr.service.impl.UserServiceImpl;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Command for aspirants list showing.
 *
 * @author Dolnikov Vladislav
 */
public class ShowAspirantsListCommand implements ActionCommand {

    /**
     * Finds aspirants in a database and redirects to the relevant page.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException, IOException
     *                          or ServletException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            UserService userService = new UserServiceImpl();
            List<User> aspirantsList = userService.getUsersByRole(UserRole.ASPIRANT);
            request.setAttribute("aspirantsList", aspirantsList);

            PageDispatcher.doForward(request, response, Pages.ASPIRANTS_LIST.getUrl());
        } catch (ServiceException | IOException | ServletException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
