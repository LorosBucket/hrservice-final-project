package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.entity.vacancy.Vacancy;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.VacancyService;
import com.epam.hr.service.impl.VacancyServiceImpl;
import com.epam.hr.util.PageDispatcher;
import com.epam.hr.util.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Command for vacancies with subscribers showing.
 *
 * @author Dolnikov Vladislav
 */
public class ShowVacanciesWithResponsesCommand implements ActionCommand {

    /**
     * Finds vacancies with subscribers in a database
     * using vacancy service and displays them.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException,
     *                          ServletException or IOException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        try {
            VacancyService vacancyService = new VacancyServiceImpl();
            List<Vacancy> vacanciesList = vacancyService.getVacanciesWithResponses();

            request.setAttribute("vacanciesList", vacanciesList);

            PageDispatcher.doForward(request, response, Pages.HR_HOME.getUrl());
        } catch (ServiceException | ServletException | IOException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
