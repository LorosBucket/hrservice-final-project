package com.epam.hr.command.impl;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.CommandException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.UserService;
import com.epam.hr.service.impl.UserServiceImpl;
import com.epam.hr.util.EncoderUtil;
import com.epam.hr.util.Pages;
import com.epam.hr.util.validator.LoginValidator;
import com.epam.hr.util.validator.PasswordValidator;
import com.epam.hr.util.validator.PhoneFormatValidator;
import com.epam.hr.util.validator.ValidatorUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for new user registration.
 *
 * @author Dolnikov Vladislav
 */
public class RegistrationCommand implements ActionCommand {
    private static final Logger LOGGER = LogManager.getLogger(RegistrationCommand.class);

    private static final byte IS_BLOCKED_DEFAULT = 0;
    private static final String NEW_USER = "newUser";
    private static final String MODAL_FRAME = "#modal_frame";

    private static final String LOCAL_MESSAGE_INVALID_INPUT = "local.message.invalid.input";
    private static final String ERROR_SIGN_UP_MESSAGE = "errorSignUpMessage";
    private static final String LOCAL_MESSAGE_USER_EXISTS = "local.message.user.exists";
    private static final String LOCAL_MESSAGE_INVALID_PHONE = "local.message.invalid.phone";

    private static final String LOCAL_MESSAGE_INVALID_LOGIN = "local.message.invalid.login";
    private static final String LOCAL_MESSAGE_INVALID_PASSWORD = "local.message.invalid.password";
    private static final String LOCAL_MESSAGE_SUCCESS = "local.message.success";
    private static final String LOCAL_MESSAGE_INTERNAL_ERROR = "local.message.internal.error";
    private static final String REGISTRATION_PASS_MESSAGE = "errorLoginPassMessage";

    /**
     * Gets all entered parameters from the request,
     * validates and saves new user using user service.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws CommandException if ServiceException has been thrown.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String login = request.getParameter("login");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String phone = request.getParameter("phone");
        String password = request.getParameter("pass");

        User newUser = new User(null,
                EncoderUtil.encodeToUtf8(name),
                EncoderUtil.encodeToUtf8(surname),
                EncoderUtil.encodeToUtf8(login), IS_BLOCKED_DEFAULT, phone, UserRole.ASPIRANT, password);

        boolean isValidParams = validateParams(newUser, request, response);

        if (isValidParams) {
            try {
                boolean isUniqueLogin = checkForLoginExisting(login);
                if (!isUniqueLogin) {
                    processIllegalInput(request, response, newUser, LOCAL_MESSAGE_USER_EXISTS);
                    LOGGER.debug("Registration rejected: login already exists");
                    return;
                }

                UserService userService = new UserServiceImpl();
                User user = userService.registrateUser(newUser);

                if (user != null) {
                    request.getSession().setAttribute(REGISTRATION_PASS_MESSAGE, LOCAL_MESSAGE_SUCCESS);
                    LOGGER.debug("New user has been successfully added.");
                    redirectToLogin(request, response, false);
                } else {
                    request.getSession().setAttribute(ERROR_SIGN_UP_MESSAGE, LOCAL_MESSAGE_INTERNAL_ERROR);
                    redirectToLogin(request, response, true);
                }

            } catch (ServiceException e) {
                throw new CommandException(e.getMessage(), e);
            }
        }
    }

    private boolean validateParams(User newUser, HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String login = newUser.getLogin();
        String name = newUser.getName();
        String surname = newUser.getSurname();
        String phone = newUser.getPhoneNumber();
        String password = newUser.getPassword();

        boolean notNull = ValidatorUtils.checkParamsForNull(login, name, surname, phone, password);
        boolean notEmpty = ValidatorUtils.validateOnEmptyString(login, name, surname, phone, password);
        if (!notEmpty || !notNull) {
            processIllegalInput(request, response, newUser, LOCAL_MESSAGE_INVALID_INPUT);
            LOGGER.debug("Illegal input parameters.");
            return false;
        }

        boolean isValidLogin = LoginValidator.validate(login);
        if (!isValidLogin) {
            processIllegalInput(request, response, newUser, LOCAL_MESSAGE_INVALID_LOGIN);
            LOGGER.debug("Illegal login parameter.");
            return false;
        }

        boolean isValidPassword = PasswordValidator.validate(password);
        if (!isValidPassword) {
            processIllegalInput(request, response, newUser, LOCAL_MESSAGE_INVALID_PASSWORD);
            LOGGER.debug("Illegal password parameter.");
            return false;
        }

        boolean isValidPhoneFormat = PhoneFormatValidator.validate(phone);
        if (!isValidPhoneFormat) {
            processIllegalInput(request, response, newUser, LOCAL_MESSAGE_INVALID_PHONE);
            LOGGER.debug("Illegal phone format parameter.");
            return false;
        }
        return true;
    }

    private boolean checkForLoginExisting(String login) throws ServiceException {
        UserService userService = new UserServiceImpl();
        Integer userId = userService.getUserIdByLogin(login);
        return userId == null;
    }

    private void processIllegalInput(HttpServletRequest request, HttpServletResponse response, User newUser, String message)
            throws CommandException {
        /* Illegal input params have been entered */
        request.getSession().setAttribute(ERROR_SIGN_UP_MESSAGE, message);
        request.getSession().setAttribute(NEW_USER, newUser);
        redirectToLogin(request, response, true);
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response, boolean showRegistrationForm)
            throws CommandException {
        try {
            String page = request.getContextPath() + Pages.LOGIN.getUrl();
            if (showRegistrationForm) page += MODAL_FRAME;
            response.sendRedirect(page);
        } catch (IOException e) {
            throw new CommandException(e.getMessage(), e);
        }

    }
}
