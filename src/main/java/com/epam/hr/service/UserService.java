package com.epam.hr.service;

import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.ServiceException;

import java.util.List;

public interface UserService {
    /**
     * Finds and returns user by id;
     *
     * @param id user id
     * @return user
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    User getById(Integer id) throws ServiceException;

    /**
     * Returns {@code User} if the pair login-password is valid
     * otherwise returns NULL.
     *
     * @param login    User's authentication login
     * @param password User's authentication password
     * @return User if the pair login-password is valid otherwise null
     */
    User authorizeUser(String login, String password) throws ServiceException;

    /**
     * Returns list of users with specified role.
     *
     * @param role user's role
     * @return ArrayList of users.
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    List<User> getUsersByRole(UserRole role) throws ServiceException;

    /**
     * Blocks user's account if parameter setBlock is true.
     * Unblocks user's account if parameter setBlock is false.
     * Returns true if success.
     *
     * @param userId
     * @param setBlock block account - true
     *                 unblock account - false
     * @return true if account blocking success
     * otherwise false.
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    boolean setAccountBlocking(Integer userId, boolean setBlock) throws ServiceException;

    /**
     * Search user by login and return ID if exists.
     * Otherwise return NULL.
     *
     * @param login user login
     * @return Integer user ID if user exists,
     * otherwise null
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    Integer getUserIdByLogin(String login) throws ServiceException;

    /**
     * Save new user in a database. Return object User if success,
     * otherwise null.
     *
     * @param newUser new user to registration
     * @return user if success, otherwise null
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    User registrateUser(User newUser) throws ServiceException;
}
