package com.epam.hr.service;

import com.epam.hr.entity.interview.Interview;
import com.epam.hr.entity.interview.InterviewStatus;
import com.epam.hr.entity.interview.InterviewType;
import com.epam.hr.exception.ServiceException;

import java.util.List;

public interface InterviewService {
    /**
     * Returns list of interviews by subscriberId.
     * Can return NULL.
     *
     * @return ArrayList of Interviews
     * @throws ServiceException
     */
    List<Interview> getInterviews(Integer subscriberId) throws ServiceException;

    /**
     * Deletes interview by id.
     *
     * @param interviewId interview id
     * @throws ServiceException if ConnectionPoolException or
     *                          DaoException has been thrown.
     */
    void deleteById(Integer interviewId) throws ServiceException;

    /**
     * Validates and saves interview. Can return NULL if invalid data.
     *
     * @param subscriberId subscriber id
     * @param type         interview type
     * @param status       interview status
     * @param comment      interview comment
     * @return interview if saved successful, otherwise return NULL
     * @throws ServiceException if ConnectionPoolException or
     *                          DaoException has been thrown.
     */
    Interview save(Integer subscriberId, InterviewType type, InterviewStatus status, String comment) throws ServiceException;
}

