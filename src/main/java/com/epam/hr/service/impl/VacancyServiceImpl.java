package com.epam.hr.service.impl;

import com.epam.hr.dao.dbconnection.ConnectionPool;
import com.epam.hr.dao.vacancydao.VacancyDao;
import com.epam.hr.dao.vacancydao.VacancyDaoImpl;
import com.epam.hr.entity.vacancy.Vacancy;
import com.epam.hr.exception.ConnectionPoolException;
import com.epam.hr.exception.DaoException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.VacancyService;
import com.epam.hr.util.validator.VacancyValidator;

import java.sql.Connection;
import java.util.List;

public class VacancyServiceImpl implements VacancyService {
    private static final byte DEFAULT_HIDDEN = 0;

    /**
     * Returns ArrayList of the vacancies with field "isActive" = 1
     *
     * @param pageIndex (for pagination)
     * @param count     of rows (for pagination)
     * @return ArrayList of the vacancies
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public List<Vacancy> getActiveVacancies(Integer pageIndex, Integer count) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            VacancyDao vacancyDao = new VacancyDaoImpl(connection);

            return vacancyDao.findActive(pageIndex, count);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }


    /**
     * Sets vacancy field is_active to false.
     *
     * @param id vacancy Id
     * @return true if success otherwise false.
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public boolean hideVacancy(Integer id) throws ServiceException {

        Connection connection = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connection = connectionPool.getConnection();
            VacancyDao vacancyDao = new VacancyDaoImpl(connection);

            Vacancy vacancy = vacancyDao.getById(id);
            vacancy.setIsActive(DEFAULT_HIDDEN);
            Vacancy addedVacancy = vacancyDao.save(vacancy);
            return addedVacancy != null;
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Returns all vacancies which have responses.
     *
     * @return ArrayList
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public List<Vacancy> getVacanciesWithResponses() throws ServiceException {
        Connection connection = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connection = connectionPool.getConnection();
            VacancyDao vacancyDao = new VacancyDaoImpl(connection);
            return vacancyDao.findVacanciesWithSubscribers();
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Validates and saves vacancy. Can return NULL if invalid data.
     *
     * @param vacancyId      of a vacancy
     * @param specialization of a vacancy
     * @param location       of a vacancy
     * @param description    of a vacancy
     * @param status         of a vacancy
     * @return vacancy if saved successful, otherwise return NULL
     */
    @Override
    public Vacancy save(Integer vacancyId, String specialization, String location, String description, byte status)
            throws ServiceException {

        boolean isValid = VacancyValidator.validate(specialization, location, description, status);
        if (!isValid) {
            return null;
        }

        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            VacancyDao vacancyDao = new VacancyDaoImpl(connection);
            Vacancy vacancy = new Vacancy(vacancyId, specialization, location, description, status);
            return vacancyDao.save(vacancy);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Returns vacancy by Id.
     *
     * @param vacancyId vacancy id
     * @return Vacancy if exist
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public Vacancy getVacancyById(Integer vacancyId) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            VacancyDao vacancyDao = new VacancyDaoImpl(connection);
            return vacancyDao.getById(vacancyId);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Returns vacancies amount
     *
     * @return Integer vacanciesAmount
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public Integer getVacanciesAmount() throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            VacancyDao vacancyDao = new VacancyDaoImpl(connection);
            return vacancyDao.getVacanciesAmount();
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }
}
