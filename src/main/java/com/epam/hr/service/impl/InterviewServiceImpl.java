package com.epam.hr.service.impl;

import com.epam.hr.dao.dbconnection.ConnectionPool;
import com.epam.hr.dao.interviewdao.InterviewDao;
import com.epam.hr.dao.interviewdao.InterviewDaoImpl;
import com.epam.hr.entity.interview.Interview;
import com.epam.hr.entity.interview.InterviewStatus;
import com.epam.hr.entity.interview.InterviewType;
import com.epam.hr.exception.ConnectionPoolException;
import com.epam.hr.exception.DaoException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.InterviewService;
import com.epam.hr.util.validator.InterviewValidator;
import com.epam.hr.util.validator.ValidatorUtils;

import java.sql.Connection;
import java.util.List;

public class InterviewServiceImpl implements InterviewService {

    /**
     * Returns list of interviews by subscriberId.
     * Can return NULL.
     *
     * @param subscriberId subscriber's id
     * @return ArrayList of Interviews
     * @throws ServiceException if ConnectionPoolException or
     *                          DaoException has been thrown.
     */
    @Override
    public List<Interview> getInterviews(Integer subscriberId) throws ServiceException {

        boolean isNotEmpty = ValidatorUtils.checkParamsForNull(subscriberId);
        if (!isNotEmpty) {
            throw new ServiceException("SubscriberId is null.");
        }

        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            InterviewDao interviewDao = new InterviewDaoImpl(connection);

            return interviewDao.findBySubscriberId(subscriberId);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Deletes interview by id.
     *
     * @param interviewId interview id
     * @throws ServiceException if ConnectionPoolException or
     *                          DaoException has been thrown.
     */
    @Override
    public void deleteById(Integer interviewId) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            InterviewDao interviewDao = new InterviewDaoImpl(connection);

            interviewDao.deleteById(interviewId);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Validates and saves interview. Can return NULL if invalid data.
     *
     * @param subscriberId subscriber id
     * @param type         interview type
     * @param status       interview status
     * @param comment      interview comment
     * @return interview if saved successful, otherwise return NULL
     * @throws ServiceException if ConnectionPoolException or
     *                          DaoException has been thrown.
     */
    @Override
    public Interview save(Integer subscriberId, InterviewType type, InterviewStatus status, String comment)
            throws ServiceException {

        boolean isValid = InterviewValidator.validate(comment);
        if (!isValid) {
            return null;
        }

        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            InterviewDao interviewDao = new InterviewDaoImpl(connection);
            Interview interview = new Interview(null, type, status, subscriberId);
            interview.setComment(comment);
            return interviewDao.save(interview);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }
}
