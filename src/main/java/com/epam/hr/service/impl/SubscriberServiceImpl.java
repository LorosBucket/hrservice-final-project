package com.epam.hr.service.impl;

import com.epam.hr.dao.dbconnection.ConnectionPool;
import com.epam.hr.dao.dto.SubscriptionDto;
import com.epam.hr.dao.subscriber.SubscriberDao;
import com.epam.hr.dao.subscriber.SubscriberDaoImpl;
import com.epam.hr.entity.subscriber.Subscriber;
import com.epam.hr.entity.subscriber.SubscriberStatus;
import com.epam.hr.exception.ConnectionPoolException;
import com.epam.hr.exception.DaoException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.SubscriberService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.List;

public class SubscriberServiceImpl implements SubscriberService {
    private static final Logger LOGGER = LogManager.getLogger(SubscriberServiceImpl.class);
    private static final SubscriberStatus DEFAULT_STATUS = SubscriberStatus.SUBMITTED;
    private static final byte DEFAULT_ACTIVE = 1;
    private static final byte DEFAULT_SUSPENDED = 0;

    /**
     * Returns subscriber status by subscriberId.
     *
     * @param subscriberId subscriber id
     * @return SubscriberStatus
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    @Override
    public SubscriberStatus getSubscriberStatus(Integer subscriberId)
            throws ServiceException {

        ConnectionPool connectionPool = ConnectionPool.getInstance();
        LOGGER.debug("The connectionPool has been received");
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            SubscriberDao subscriberDao = new SubscriberDaoImpl(connection);

            return subscriberDao.findSubscriberStatus(subscriberId);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Change subscriber status. Return true if success.
     *
     * @param id     of subscriber
     * @param status change to
     * @return true if changed successful, otherwise false
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    @Override
    public boolean updateSubscriberStatus(Integer id, SubscriberStatus status) throws ServiceException {
        checkParamsForNull(id, status);

        Connection connection = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connection = connectionPool.getConnection();
            SubscriberDao subscriberDao = new SubscriberDaoImpl(connection);

            Subscriber subscriber = subscriberDao.getById(id);
            subscriber.setStatus(status);
            Subscriber saved = subscriberDao.save(subscriber);
            return saved != null;
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Adds new subscriber in a database by userId and vacancyId
     * with default status or updates existing hidden subscriber.
     * Returns true if successful added or updated.
     *
     * @param userId    user's id
     * @param vacancyId vacancy id
     * @return true if successful added or updated, otherwise false
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    @Override
    public boolean addNewSubscriber(Integer userId, Integer vacancyId)
            throws ServiceException {
        checkParamsForNull(userId, vacancyId);

        Connection connection = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();

        try {
            connection = connectionPool.getConnection();
            SubscriberDao subscriberDao = new SubscriberDaoImpl(connection);

            Integer subscriberId = subscriberDao.findSubscriber(userId, vacancyId);
            /*subscriberId is null when subscriber is not found in a database*/
            if (subscriberId == null) {
                Subscriber subscriber = new Subscriber(null, DEFAULT_ACTIVE, vacancyId, userId, DEFAULT_STATUS);
                Subscriber addedSubscriber = subscriberDao.save(subscriber);
                return addedSubscriber != null;
            } else {
                Subscriber subscriber = subscriberDao.getById(subscriberId);
                byte isActive = subscriber.getIsActive();
                /*Set subscriber status to active, if it was hidden*/
                if (isActive != DEFAULT_ACTIVE) {
                    subscriber.setIsActive(DEFAULT_ACTIVE);
                    subscriber.setStatus(DEFAULT_STATUS);
                    Subscriber addedSubscriber = subscriberDao.save(subscriber);
                    return addedSubscriber != null;
                }
            }
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
        return false;
    }

    /**
     * Finds subscriber by userId and vacancyId, then
     * sets subscriber field is_active to false by subscriberId.
     *
     * @param userId    user's id
     * @param vacancyId vacancy id
     * @return true if success otherwise false.
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    @Override
    public boolean hideSubscriber(Integer userId, Integer vacancyId) throws ServiceException {
        checkParamsForNull(userId, vacancyId);

        Connection connection = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connection = connectionPool.getConnection();
            SubscriberDao subscriberDao = new SubscriberDaoImpl(connection);

            Integer subscriberId = subscriberDao.findSubscriber(userId, vacancyId);
            /*subscriberId is null when subscriber is not found in a database*/
            if (subscriberId != null) {
                Subscriber subscriber = subscriberDao.getById(subscriberId);
                subscriber.setIsActive(DEFAULT_SUSPENDED);
                Subscriber addedSubscriber = subscriberDao.save(subscriber);
                return addedSubscriber != null;
            }
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
        return false;
    }

    /**
     * Returns all active aspirant's subscription with vacancies.
     *
     * @param id User(aspirant) Id
     * @return ArrayList of subscriptionDto.
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    @Override
    public List<SubscriptionDto> getAspirantSubscriptions(Integer id) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            SubscriberDao subscriberDao = new SubscriberDaoImpl(connection);

            return subscriberDao.findSubscriptionsByUserId(id);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Returns all active vacancy subscribers.
     *
     * @param id vacancy Id
     * @return ArrayList of subscriptionDto.
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    @Override
    public List<SubscriptionDto> getVacancySubscribers(Integer id) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            SubscriberDao subscriberDao = new SubscriberDaoImpl(connection);

            return subscriberDao.findSubscriptionsByVacancyId(id);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    private void checkParamsForNull(Object firstParam, Object secondParam) throws ServiceException {
        if (firstParam == null || secondParam == null) {
            throw new ServiceException("Service action error. Parameter(s) is null");
        }
    }
}
