package com.epam.hr.service.impl;

import com.epam.hr.dao.dbconnection.ConnectionPool;
import com.epam.hr.dao.userdao.UserDao;
import com.epam.hr.dao.userdao.UserDaoImpl;
import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.ConnectionPoolException;
import com.epam.hr.exception.DaoException;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.service.UserService;

import java.sql.Connection;
import java.util.List;

public class UserServiceImpl implements UserService {
    private static final byte DEFAULT_BLOCKED = 1;
    private static final byte DEFAULT_UNBLOCKED = 0;

    /**
     * Finds and returns user by id;
     *
     * @param id user id
     * @return user
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public User getById(Integer id) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            UserDao userDao = new UserDaoImpl(connection);
            return userDao.getById(id);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Returns {@code User} if the pair login-password is valid
     * otherwise returns NULL.
     *
     * @param login    User's authentication login
     * @param password User's authentication password
     * @return User if the pair login-password is valid otherwise null
     */
    @Override
    public User authorizeUser(String login, String password) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            UserDao userDao = new UserDaoImpl(connection);
            return userDao.findByLoginAndPassword(login, password);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Returns list of users with specified role.
     *
     * @param role user's role
     * @return ArrayList of users.
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public List<User> getUsersByRole(UserRole role) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            UserDao userDao = new UserDaoImpl(connection);
            return userDao.findUsersByRole(role);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Blocks user's account if parameter setBlock is true.
     * Unblocks user's account if parameter setBlock is false.
     * Returns true if success.
     *
     * @param userId   user's id
     * @param setBlock block account - true
     *                 unblock account - false
     * @return true if account blocking success
     * otherwise false.
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public boolean setAccountBlocking(Integer userId, boolean setBlock) throws ServiceException {
        checkIdForNull(userId);

        Connection connection = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connection = connectionPool.getConnection();
            UserDao userDao = new UserDaoImpl(connection);

            User user = userDao.getById(userId);
            if (setBlock) {
                user.setIsBlocked(DEFAULT_BLOCKED);
            } else {
                user.setIsBlocked(DEFAULT_UNBLOCKED);

            }
            User addedUser = userDao.save(user);
            return addedUser != null;
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Finds user by login and returns id if exists.
     * Otherwise returns null.
     *
     * @param login user login
     * @return user's id
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public Integer getUserIdByLogin(String login) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            UserDao userDao = new UserDaoImpl(connection);
            return userDao.findIdByLogin(login);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    /**
     * Save new user in a database. Return object User if success,
     * otherwise null.
     *
     * @param newUser new user to registration
     * @return user if success, otherwise null
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    @Override
    public User registrateUser(User newUser) throws ServiceException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            UserDao userDao = new UserDaoImpl(connection);
            return userDao.save(newUser);
        } catch (ConnectionPoolException | DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    private void checkIdForNull(Integer userId) throws ServiceException {
        if (userId == null) {
            throw new ServiceException("Service action error. UserId is null");
        }
    }
}
