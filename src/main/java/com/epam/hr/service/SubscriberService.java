package com.epam.hr.service;

import com.epam.hr.entity.subscriber.SubscriberStatus;
import com.epam.hr.exception.ServiceException;
import com.epam.hr.dao.dto.SubscriptionDto;

import java.util.List;

public interface SubscriberService {

    /**
     * Returns subscriber status by subscriberId.
     * @return SubscriberStatus
     */
    SubscriberStatus getSubscriberStatus(Integer subscriberId) throws ServiceException;

    /**
     * Change subscriber status. Return true if success.
     *
     * @param id     of subscriber
     * @param status change to
     * @return true if changed successful, otherwise false
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    boolean updateSubscriberStatus(Integer id, SubscriberStatus status) throws ServiceException;

    /**
     * Adds new subscriber in a database by userId and vacancyId
     * with default status or updates existing hidden subscriber.
     * Returns true if successful added or updated.
     *
     * @param userId    user's id
     * @param vacancyId vacancy id
     * @return true if successful added or updated, otherwise false
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    boolean addNewSubscriber(Integer userId, Integer vacancyId) throws ServiceException;

    /**
     * Finds subscriber by userId and vacancyId, then
     * sets subscriber field is_active to false by subscriberId.
     *
     * @param userId    user's id
     * @param vacancyId vacancy id
     * @return true if success otherwise false.
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    boolean hideSubscriber(Integer userId, Integer vacancyId) throws ServiceException;

    /**
     * Returns all active aspirant's subscription with vacancies.
     *
     * @param id User(aspirant) Id
     * @return ArrayList of subscriptionDto.
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    List<SubscriptionDto> getAspirantSubscriptions(Integer id) throws ServiceException;


    /**
     * Returns all active vacancy subscribers.
     *
     * @param id vacancy Id
     * @return ArrayList of subscriptionDto.
     * @throws ServiceException if ConnectionPoolException or DaoException
     *                          has been thrown.
     */
    List<SubscriptionDto> getVacancySubscribers(Integer id) throws ServiceException;
}
