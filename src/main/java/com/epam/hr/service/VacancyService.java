package com.epam.hr.service;

import com.epam.hr.entity.vacancy.Vacancy;
import com.epam.hr.exception.ServiceException;

import java.util.List;

public interface VacancyService {
    /**
     * Returns ArrayList of the vacancies with field "isActive" = 1
     *
     * @param pageIndex (for pagination)
     * @param count     of rows (for pagination)
     * @return ArrayList of the vacancies
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    List<Vacancy> getActiveVacancies(Integer pageIndex, Integer count) throws ServiceException;

    /**
     * Sets vacancy field is_active to false.
     *
     * @param id vacancy Id
     * @return true if success otherwise false.
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    boolean hideVacancy(Integer id) throws ServiceException;

    /**
     * Returns all vacancies which have responses.
     *
     * @return ArrayList
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    List<Vacancy> getVacanciesWithResponses() throws ServiceException;

    /**
     * Validates and saves vacancy. Can return NULL if invalid data.
     *
     * @param vacancyId      of a vacancy
     * @param specialization of a vacancy
     * @param location       of a vacancy
     * @param description    of a vacancy
     * @param status         of a vacancy
     * @return vacancy if saved successful, otherwise return NULL
     */
    Vacancy save(Integer vacancyId, String specialization, String location, String description, byte status)
            throws ServiceException;

    /**
     * Returns vacancy by Id.
     *
     * @param vacancyId vacancy id
     * @return Vacancy if exist
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    Vacancy getVacancyById(Integer vacancyId) throws ServiceException;

    /**
     * Returns vacancies amount
     *
     * @return Integer vacanciesAmount
     * @throws ServiceException if ConnectionPoolException
     *                          or DaoException has been thrown.
     */
    Integer getVacanciesAmount() throws ServiceException;

}
