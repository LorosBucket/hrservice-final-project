package com.epam.hr.filter;

import com.epam.hr.command.CommandEnum;
import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.ProhibitedActionException;
import com.epam.hr.util.Pages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Confirms permission to command.
 * Redirects to safe pages in case of exceeding the role.
 *
 * @author Dolnikov Vladislav
 */
public class CommandSecurityFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(CommandSecurityFilter.class);

    private static final String USER = "user";

    private Set<CommandEnum> aspirantCommands;
    private Set<CommandEnum> hrCommands;
    private Set<CommandEnum> adminCommands;
    private Set<CommandEnum> safeCommands;

    public CommandSecurityFilter() {
        aspirantCommands = new HashSet<>();
        hrCommands = new HashSet<>();
        adminCommands = new HashSet<>();
        safeCommands = new HashSet<>();
    }

    @Override
    public void init(FilterConfig filterConfig) {
        fillAspirantCommands();
        fillHrCommands();
        fillAdminCommands();
        fillSafeCommands();
    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        HttpSession session = httpRequest.getSession(false);

        if (session == null) {
            LOGGER.debug("Command filter: session is null. Redirecting to index page.");
            redirectNotAuthorizedAccess(httpRequest, httpResponse);
            return;
        }

        String command = httpRequest.getParameter("command");
        LOGGER.debug("Command filter: command request: " + command);

        try {
            if (command != null) {
                User user = (User) session.getAttribute(USER);
                UserRole role = null;

                if (user != null) {
                    role = user.getRole();
                }

                handleWithRole(command.toUpperCase(), role);
            }
        } catch (ProhibitedActionException e) {
            LOGGER.debug("Command filter: prohibited action.");
            redirectToErrorPage(httpRequest, httpResponse);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void handleWithRole(String command, UserRole role) throws ProhibitedActionException {
        CommandEnum commandEnum = CommandEnum.valueOf(command);

        if (role == null){
            LOGGER.debug("Handling action without role (safe commands)");
            if (!safeCommands.contains(commandEnum)) {
                throw new ProhibitedActionException();
            }

        } else {
            switch (role) {
                case ASPIRANT: {
                    if (!aspirantCommands.contains(commandEnum)) {
                        throw new ProhibitedActionException();
                    }
                    break;
                }
                case HR: {
                    if (!hrCommands.contains(commandEnum)) {
                        throw new ProhibitedActionException();
                    }
                    break;
                }
                case ADMIN: {
                    if (!adminCommands.contains(commandEnum)) {
                        throw new ProhibitedActionException();
                    }
                    break;
                }
                default: {
                    throw new ProhibitedActionException();
                }
            }
        }
    }

    private void redirectNotAuthorizedAccess(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        httpResponse.sendRedirect(httpRequest.getContextPath() + Pages.INDEX.getUrl());
    }

    private void redirectToErrorPage(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        httpResponse.sendRedirect(httpRequest.getContextPath() + Pages.ERROR.getUrl());
    }

    private void fillSafeCommands() {
        safeCommands.add(CommandEnum.LOGIN);
        safeCommands.add(CommandEnum.CHANGE_LOCAL);
        safeCommands.add(CommandEnum.LOGOUT);
        safeCommands.add(CommandEnum.REGISTRATION);
    }

    private void fillAdminCommands() {
        adminCommands.add(CommandEnum.LOGOUT);
        adminCommands.add(CommandEnum.LOGIN);
        adminCommands.add(CommandEnum.CHANGE_LOCAL);
        adminCommands.add(CommandEnum.SHOW_ASPIRANTS_LIST);
        adminCommands.add(CommandEnum.SHOW_HRS_LIST);
        adminCommands.add(CommandEnum.SHOW_VACANCIES);
        adminCommands.add(CommandEnum.BLOCK_ACCOUNT);
        adminCommands.add(CommandEnum.UNBLOCK_ACCOUNT);
    }

    private void fillHrCommands() {
        hrCommands.add(CommandEnum.LOGIN);
        hrCommands.add(CommandEnum.LOGOUT);
        hrCommands.add(CommandEnum.CHANGE_LOCAL);
        hrCommands.add(CommandEnum.SUBSCRIBER_CONTROL);
        hrCommands.add(CommandEnum.SHOW_VACANCY_SUBSCRIBERS);
        hrCommands.add(CommandEnum.UPDATE_SUBSCRIBER_STATUS);
        hrCommands.add(CommandEnum.SHOW_VACANCIES_WITH_RESPONSES);
        hrCommands.add(CommandEnum.SHOW_VACANCIES);
        hrCommands.add(CommandEnum.ADD_INTERVIEW);
        hrCommands.add(CommandEnum.ADD_VACANCY);
        hrCommands.add(CommandEnum.DELETE_INTERVIEW);
        hrCommands.add(CommandEnum.EDIT_VACANCY);
        hrCommands.add(CommandEnum.HIDE_VACANCY);
    }

    private void fillAspirantCommands() {
        aspirantCommands.add(CommandEnum.LOGIN);
        aspirantCommands.add(CommandEnum.LOGOUT);
        aspirantCommands.add(CommandEnum.CHANGE_LOCAL);
        aspirantCommands.add(CommandEnum.SHOW_ASPIRANT_VACANCIES);
        aspirantCommands.add(CommandEnum.SHOW_VACANCIES);
        aspirantCommands.add(CommandEnum.ADD_SUBSCRIBER);
        aspirantCommands.add(CommandEnum.HIDE_SUBSCRIBER);
    }
}
