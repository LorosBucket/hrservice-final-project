package com.epam.hr.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Filter encodes data to UTF-8
 *
 * @author Dolnikov Vladislav
 */
public class EncodingFilter implements Filter {

    private static final String ENCODING = "encoding";
    private String code;

    @Override
    public void init(FilterConfig filterConfig) {
        code = filterConfig.getInitParameter(ENCODING);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        request.setCharacterEncoding(code);

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        code = null;
    }

}
