package com.epam.hr.filter;

import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.ProhibitedActionException;
import com.epam.hr.util.Pages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Confirms permission to action or request.
 * Redirects to safe pages in case of exceeding the role.
 *
 * @author Dolnikov Vladislav
 */
public class PageRedirectSecurityFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(PageRedirectSecurityFilter.class);

    private static final String USER = "user";

    private List<String> aspirantPages;
    private List<String> hrPages;
    private List<String> adminPages;

    public PageRedirectSecurityFilter() {
        aspirantPages = new ArrayList<>();
        hrPages = new ArrayList<>();
        adminPages = new ArrayList<>();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        fillAspirantPages();
        fillHrPages();
        fillAdminPages();
    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        String uri = httpRequest.getRequestURI();
        HttpSession session = httpRequest.getSession(false);

        if (session == null) {
            LOGGER.info("Session is null.");
            redirectNotAuthorizedAccess(httpRequest, httpResponse);
            return;
        }

        try {
            if (uri != null) {
                User user = (User) session.getAttribute(USER);
                if (user != null) {
                    LOGGER.info("Access by the role validation.");
                    redirectByRole(httpRequest, user.getRole());
                } else {
                    LOGGER.info("User role is null.");
                    redirectNotAuthorizedAccess(httpRequest, httpResponse);
                    return;
                }
            }
        } catch (ProhibitedActionException e) {
            redirectToErrorPage(httpRequest, httpResponse);
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void redirectByRole(HttpServletRequest httpRequest, UserRole role)
            throws ProhibitedActionException {
        switch (role) {
            case ASPIRANT: {
                if (!isListContains(httpRequest, aspirantPages)) {
                    throw new ProhibitedActionException();
                }
                break;
            }
            case HR: {
                if (!isListContains(httpRequest, hrPages)) {
                    throw new ProhibitedActionException();
                }
                break;
            }
            case ADMIN: {
                if (!isListContains(httpRequest, adminPages)) {
                    throw new ProhibitedActionException();
                }
                break;
            }
            default: {
                throw new ProhibitedActionException();
            }
        }
    }

    private boolean isListContains(HttpServletRequest httpRequest, List<String> pages) {
        String uri = httpRequest.getRequestURI();
        for (String page : pages) {
            if (uri.contains(page)) {
                return true;
            }
        }
        return false;
    }

    private void fillAdminPages() {
        adminPages.add(Pages.VACANCIES_LIST.getUrl());
        adminPages.add(Pages.HRS_LIST.getUrl());
        adminPages.add(Pages.ASPIRANTS_LIST.getUrl());
        adminPages.add(Pages.LOGIN.getUrl());
        adminPages.add(Pages.INDEX.getUrl());
        adminPages.add(Pages.ERROR.getUrl());
        adminPages.add(Pages.SERVER_ERROR.getUrl());
    }

    private void fillHrPages() {
        hrPages.add(Pages.HR_HOME.getUrl());
        hrPages.add(Pages.VACANCY_SUBSCRIBERS.getUrl());
        hrPages.add(Pages.SUBSCRIBER_CONTROL.getUrl());
        hrPages.add(Pages.VACANCIES_LIST.getUrl());
        hrPages.add(Pages.LOGIN.getUrl());
        hrPages.add(Pages.INDEX.getUrl());
        hrPages.add(Pages.ERROR.getUrl());
        hrPages.add(Pages.SERVER_ERROR.getUrl());
    }

    private void fillAspirantPages() {
        aspirantPages.add(Pages.ASPIRANT_HOME.getUrl());
        aspirantPages.add(Pages.ASPIRANT_SUBSCRIPTIONS.getUrl());
        aspirantPages.add(Pages.VACANCIES_LIST.getUrl());
        aspirantPages.add(Pages.LOGIN.getUrl());
        aspirantPages.add(Pages.INDEX.getUrl());
        aspirantPages.add(Pages.ERROR.getUrl());
        aspirantPages.add(Pages.SERVER_ERROR.getUrl());
    }

    private void redirectNotAuthorizedAccess(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        LOGGER.info("Not authorized access, redirecting: index.jsp");
        httpResponse.sendRedirect(httpRequest.getContextPath() + Pages.INDEX.getUrl());
    }

    private void redirectToErrorPage(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        httpResponse.sendRedirect(httpRequest.getContextPath() + Pages.ERROR.getUrl());
    }
}
