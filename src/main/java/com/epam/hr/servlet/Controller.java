package com.epam.hr.servlet;

import com.epam.hr.command.ActionCommand;
import com.epam.hr.command.ActionFactory;
import com.epam.hr.dao.dbconnection.ConnectionPool;
import com.epam.hr.exception.CommandException;
import com.epam.hr.util.Pages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The {@code Controller} class receives and invokes
 * appropriate commands.
 *
 * @author Dolnikov Vladislav
 *
 */
public class Controller extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.debug("Method doGet execution.");
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.debug("Method doPost execution.");
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request,HttpServletResponse response)
            throws IOException {
        ActionFactory client = new ActionFactory();

        try {
            ActionCommand command = client.defineCommand(request);
            LOGGER.debug("Command execution: " + command.getClass());

            command.execute(request,response);
        } catch (CommandException e) {
            LOGGER.error(e.getMessage(), e);

            String page = request.getContextPath() + Pages.SERVER_ERROR.getUrl();
            response.sendRedirect(page);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.closePool();
    }
}
