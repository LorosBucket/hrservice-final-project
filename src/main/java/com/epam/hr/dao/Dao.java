package com.epam.hr.dao;

import com.epam.hr.entity.AbstractEntity;
import com.epam.hr.exception.DaoException;

import java.util.List;

public interface Dao<T extends AbstractEntity> {
    /**
     * Saves object in a database
     */
    T save(T object) throws DaoException;

    /**
     * Returns list of all objects in the table
     */
    List<T> getAll() throws DaoException;

    /**
     * Returns object from database by id
     */
    T getById(int id) throws DaoException;

    /**
     * Deletes object in a database
     */
    void deleteById(int id) throws DaoException;

}
