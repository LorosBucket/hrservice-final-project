package com.epam.hr.dao.interviewdao;

import com.epam.hr.dao.AbstractDao;
import com.epam.hr.entity.interview.Interview;
import com.epam.hr.entity.interview.InterviewStatus;
import com.epam.hr.entity.interview.InterviewType;
import com.epam.hr.exception.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Data access object for a interview entity.
 *
 * @author Dolnikov Vladislav
 */
public class InterviewDaoImpl extends AbstractDao<Interview> implements InterviewDao {

    private static final String SQL_SELECT_ALL_INTERVIEWS = "SELECT * FROM interview";
    private static final String SQL_INSERT_INTERVIEW = "INSERT INTO interview(id, type," +
            " status, comment, subscriber_id) VALUES (NULL,?,?,?,?)";

    private static final String SQL_SELECT_BY_ID = "SELECT * FROM interview WHERE id = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM interview WHERE id = ?";

    private static final String SQL_UPDATE_BY_ID = "UPDATE interview SET type=?, status=?," +
            " comment=?, subscriber_id=? WHERE id=?";
    private static final String SQL_GET_BY_SUBSCRIBER_ID = "SELECT * FROM interview" +
            " WHERE subscriber_id = ?";

    public InterviewDaoImpl(Connection connection) {
        super(connection);
    }

    /**
     * Returns interviews if exists in a database.
     * Otherwise returns null.
     * @param subscriberId Integer subscriber's Id
     * @return ArrayList of interviews with subscriberId
     * @throws DaoException if SQLException
     */
    @Override
    public List<Interview> findBySubscriberId(Integer subscriberId) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_BY_SUBSCRIBER_ID);
            preparedStatement.setInt(1, subscriberId);
            return executeSelectQuery(preparedStatement);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    @Override
    protected PreparedStatement getSavePreparedStatement(Interview object) throws DaoException, SQLException {

        String typeLowerCase = typeToLowerCaseString(object);
        String statusLowerCase = statusToLowerCaseString(object);

        String comment = object.getComment();
        Integer subscriberId = object.getSubscriberId();

        Integer id = object.getEntityId();
        PreparedStatement preparedStatement;

        if (id != null) {
            preparedStatement = getPreparedStatement(SQL_UPDATE_BY_ID);
            preparedStatement.setInt(5, id);
        } else {
            preparedStatement = getPreparedStatement(SQL_INSERT_INTERVIEW);
        }

        preparedStatement.setString(1, typeLowerCase);
        preparedStatement.setString(2, statusLowerCase);
        preparedStatement.setString(3, comment);
        preparedStatement.setInt(4, subscriberId);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getAllPreparedStatement() throws SQLException {
        return getPreparedStatement(SQL_SELECT_ALL_INTERVIEWS);
    }

    @Override
    protected PreparedStatement getByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_SELECT_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getDeleteByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_DELETE_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }

    @Override
    protected Interview build(ResultSet resultSet) throws SQLException {
        String typeString = resultSet.getString("type");
        String typeUpperCase = typeString.toUpperCase();
        InterviewType type = InterviewType.valueOf(typeUpperCase);

        String statusString = resultSet.getString("status");
        String statusUpperCase = statusString.toUpperCase();
        InterviewStatus status = InterviewStatus.valueOf(statusUpperCase);

        Integer id = resultSet.getInt("id");
        Integer subscriberId = resultSet.getInt("subscriber_id");

        Interview interview = new Interview(id, type, status, subscriberId);

        String comment = resultSet.getString("comment");
        if (comment != null) {
            interview.setComment(comment);
        }
        return interview;
    }

    private String typeToLowerCaseString(Interview interview) throws DaoException {
        InterviewType type = interview.getType();
        if (type != null) {
            String typeString = type.toString();
            return typeString.toLowerCase();
        }
        throw new DaoException("Field 'type' of the object 'interview' is null.");
    }

    private String statusToLowerCaseString(Interview interview) throws DaoException {
        InterviewStatus status = interview.getStatus();
        if (status != null) {
            String typeString = status.toString();
            return typeString.toLowerCase();
        }
        throw new DaoException("Field 'status' of the object 'interview' is null.");
    }
}
