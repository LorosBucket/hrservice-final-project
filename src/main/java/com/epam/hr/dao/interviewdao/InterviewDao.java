package com.epam.hr.dao.interviewdao;

import com.epam.hr.dao.Dao;
import com.epam.hr.entity.interview.Interview;
import com.epam.hr.exception.DaoException;

import java.util.List;

public interface InterviewDao extends Dao<Interview> {

    /**
     * Returns interviews if exists in a database.
     * Otherwise returns null.
     * @param subscriberId Integer subscriber's Id
     * @return ArrayList of interviews with subscriberId
     * @throws DaoException if SQLException
     */
    List<Interview> findBySubscriberId(Integer subscriberId) throws DaoException;
}
