package com.epam.hr.dao.vacancydao;

import com.epam.hr.dao.Dao;
import com.epam.hr.entity.vacancy.Vacancy;
import com.epam.hr.exception.DaoException;

import java.util.List;

public interface VacancyDao extends Dao<Vacancy> {

    /**
     * Returns all active vacancies.
     * @param pageIndex (for pagination)
     * @param count (for pagination)
     * @return vacanciesList
     * @throws DaoException if SQLException
     */
    List<Vacancy> findActive(Integer pageIndex, Integer count) throws DaoException;

    /**
     * Returns all vacancies which have subscribers.
     *
     * @return ArrayList of vacancies.
     * @throws DaoException if SQLException
     */
    List<Vacancy> findVacanciesWithSubscribers() throws DaoException;

    /**
     * Returns vacancies Amount. Can return NULL
     * @return integer vacancies Amount
     * @throws DaoException if SQLException
     */
    Integer getVacanciesAmount() throws DaoException;
}
