package com.epam.hr.dao.vacancydao;

import com.epam.hr.dao.AbstractDao;
import com.epam.hr.entity.vacancy.Vacancy;
import com.epam.hr.exception.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class VacancyDaoImpl extends AbstractDao<Vacancy> implements VacancyDao {

    private static final String SQL_INSERT_VACANCY = "INSERT INTO vacancy(id, specialization, location," +
            " description, is_active) VALUES (NULL,?,?,?,?)";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM vacancy WHERE id=?";
    private static final String SQL_UPDATE_BY_ID = "UPDATE vacancy SET specialization=?, location=?," +
            " description=?, is_active=? WHERE id=?";

    private static final String SQL_GET_BY_ID = "SELECT * FROM vacancy WHERE id=?";
    private static final String SQL_GET_ALL_VACANCIES = "SELECT * FROM vacancy";
    private static final String SQL_GET_ACTIVE = "SELECT * FROM vacancy WHERE is_active=1 LIMIT ? OFFSET ?";

    private static final String SQL_GET_VACANCIES_WITH_SUBSCRIBERS = "SELECT * FROM subscriber" +
            " INNER JOIN vacancy ON vacancy.id = subscriber.vacancy_id INNER JOIN client ON" +
            " client.id=subscriber.client_id WHERE vacancy.is_active=1 AND subscriber.is_active=1" +
            " AND client.is_blocked=0 GROUP BY vacancy.id";

    private static final String SQL_GET_VACANCIES_COUNT = "SELECT COUNT(*) AS count FROM vacancy WHERE vacancy.is_active=1";

    private static final String COUNT = "count";


    public VacancyDaoImpl(Connection connection) {
        super(connection);
    }

    /**
     * Returns all active vacancies.
     *
     * @param pageIndex (for pagination)
     * @param count     (for pagination)
     * @return vacanciesList
     * @throws DaoException if SQLException
     */
    @Override
    public List<Vacancy> findActive(Integer pageIndex, Integer count) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_ACTIVE);

            preparedStatement.setInt(1, count);
            preparedStatement.setInt(2, count * (pageIndex - 1));

            return executeSelectQuery(preparedStatement);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Returns all vacancies which have subscribers.
     *
     * @return ArrayList of vacancies.
     * @throws DaoException if SQLException
     */
    @Override
    public List<Vacancy> findVacanciesWithSubscribers() throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_VACANCIES_WITH_SUBSCRIBERS);

            return executeSelectQuery(preparedStatement);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Returns vacancies Amount. Can return NULL
     *
     * @return integer vacancies Amount
     * @throws DaoException if SQLException
     */
    @Override
    public Integer getVacanciesAmount() throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_VACANCIES_COUNT);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            return resultSet.getInt(COUNT);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    @Override
    protected PreparedStatement getSavePreparedStatement(Vacancy object)
            throws SQLException {

        String specialization = object.getSpecialization();
        String location = object.getLocation();
        String description = object.getDescription();

        byte isActive = object.getIsActive();
        Integer id = object.getEntityId();

        PreparedStatement preparedStatement;

        if (id != null) {
            preparedStatement = getPreparedStatement(SQL_UPDATE_BY_ID);
            preparedStatement.setInt(5, id);
        } else {
            preparedStatement = getPreparedStatement(SQL_INSERT_VACANCY);
        }

        preparedStatement.setString(1, specialization);
        preparedStatement.setString(2, location);
        preparedStatement.setString(3, description);
        preparedStatement.setByte(4, isActive);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getAllPreparedStatement() throws SQLException {
        return getPreparedStatement(SQL_GET_ALL_VACANCIES);
    }

    @Override
    protected PreparedStatement getByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getDeleteByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_DELETE_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }

    @Override
    protected Vacancy build(ResultSet resultSet) throws SQLException {
        String specialization = resultSet.getString("specialization");
        String location = resultSet.getString("location");
        String description = resultSet.getString("description");

        byte isActive = resultSet.getByte("is_active");
        Integer id = resultSet.getInt("vacancy.id");

        return new Vacancy(id, specialization, location, description, isActive);
    }
}