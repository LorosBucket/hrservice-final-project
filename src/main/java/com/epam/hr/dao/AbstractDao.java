package com.epam.hr.dao;

import com.epam.hr.entity.AbstractEntity;
import com.epam.hr.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines mutual methods and field for all concrete Dao.
 *
 * @param <T> any business entity.
 */
public abstract class AbstractDao<T extends AbstractEntity> implements Dao<T> {
    private static final Logger LOGGER = LogManager.getLogger(AbstractDao.class);

    private Connection connection;

    protected AbstractDao(Connection connection) {
        this.connection = connection;
    }

    protected abstract PreparedStatement getSavePreparedStatement(T object) throws DaoException, SQLException;

    protected abstract PreparedStatement getAllPreparedStatement() throws DaoException, SQLException;

    protected abstract PreparedStatement getByIdPreparedStatement(int id) throws DaoException, SQLException;

    protected abstract PreparedStatement getDeleteByIdPreparedStatement(int id) throws DaoException, SQLException;

    protected abstract T build(ResultSet resultSet) throws SQLException;

    /**
     * Insets object in a database.
     *
     * @param object any business entity
     * @return saved object
     * @throws DaoException if SQLException
     *                      has been thrown.
     */
    public T save(T object) throws DaoException {
        try {
            PreparedStatement preparedStatement = getSavePreparedStatement(object);

            preparedStatement.executeUpdate();

            Integer id = object.getEntityId();
            if (id == null) {
                id = getGeneratedId(preparedStatement);
                object.setEntityId(id);
            }

            LOGGER.info("Object has been updated in the database: " + object.toString());
            return object;
        } catch (SQLException e) {
            throw new DaoException("Object hasn't been updated in the database. " + e.getMessage(), e);
        }
    }

    /**
     * Returns all object from a database of a
     * concrete type.
     *
     * @return List of objects
     * @throws DaoException if SQLException
     *                      has been thrown.
     */
    public List<T> getAll() throws DaoException {
        List<T> list = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getAllPreparedStatement();

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                T entity = build(resultSet);
                list.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return list;
    }

    /**
     * Finds and returns entity by id.
     * Return null if does not exist.
     *
     * @param id entity id
     * @return entity
     * @throws DaoException if SQLException
     *                      has been thrown.
     */
    public T getById(int id) throws DaoException {
        try {
            PreparedStatement preparedStatement = getByIdPreparedStatement(id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return build(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        throw new DaoException("Interview with ID = " + id + " is not found.");
    }

    /**
     * Deletes entity by id.
     *
     * @param id entity id
     * @throws DaoException if SQLException
     *                      has been thrown.
     */
    public void deleteById(int id) throws DaoException {
        try {
            PreparedStatement preparedStatement = getDeleteByIdPreparedStatement(id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Object with ID = " + id + " hasn't been deleted: " + e.getMessage(), e);
        }
    }

    protected List<T> executeSelectQuery(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.executeQuery();
        List<T> list = new ArrayList<>();
        while (resultSet.next()) {
            T object = build(resultSet);
            list.add(object);
        }
        return list;
    }

    protected PreparedStatement getPreparedStatement(String query) throws SQLException {
        return connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
    }

    private int getGeneratedId(PreparedStatement preparedStatement) throws DaoException, SQLException {
        ResultSet resultSet = preparedStatement.getGeneratedKeys();
        if (resultSet.next()) {
            return resultSet.getInt(1);
        }
        throw new DaoException("Generated key 'id' is not found in the database.");
    }
}
