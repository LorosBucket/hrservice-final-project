package com.epam.hr.dao.userdao;

import com.epam.hr.dao.Dao;
import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.DaoException;

import java.sql.PreparedStatement;
import java.util.List;

/**
 * Defines additional methods for working with data for userDao.
 *
 * @author Dolnikov Vladislav
 */
public interface UserDao extends Dao<User> {

    /**
     * Returns a user if exists in a database.
     * Can return NULL!
     *
     * @param login user login.
     * @param password user password.
     * @return user if the pair login-password exists in a database
     *          or return NULL otherwise.
     * @throws DaoException
     *          If PreparedStatement.setString throws SQLException
     *          @see PreparedStatement
     */
    User findByLoginAndPassword(String login, String password) throws DaoException;

    /**
     * Selects and returns all users with specified role.
     * @param role required user's role
     * @return ArrayList of users
     * @throws DaoException if SQLException
     */
    List<User> findUsersByRole(UserRole role) throws DaoException;

    /**
     * Selects and returns user id by login.
     * Returns null if does not exist.
     * @param login user login
     * @return Integer user id
     * @throws DaoException if SQLException
     */
    Integer findIdByLogin(String login) throws DaoException;
}
