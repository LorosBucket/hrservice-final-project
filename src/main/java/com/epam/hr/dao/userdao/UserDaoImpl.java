package com.epam.hr.dao.userdao;

import com.epam.hr.dao.AbstractDao;
import com.epam.hr.entity.user.User;
import com.epam.hr.entity.user.UserRole;
import com.epam.hr.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserDaoImpl extends AbstractDao<User> implements UserDao {
    private static final Logger LOGGER = LogManager.getLogger(UserDaoImpl.class);

    private static final String SQL_SELECT_ALL_USERS = "SELECT * FROM client INNER" +
            " JOIN role ON role.id = client.role_id";
    private static final String SQL_SELECT_BY_ROLE = "SELECT * FROM client INNER" +
            " JOIN role ON role.id = client.role_id WHERE role.role_name=?";
    private static final String SQL_INSERT_USER = "INSERT INTO client(id, name, surname, login," +
            " is_blocked, phone_number, role_id, password)" +
            " VALUES (NULL,?,?,?,?,?,(SELECT id FROM role where role_name=?),md5(?))";
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM client INNER JOIN" +
            " role ON role.id = client.role_id WHERE client.id = ?";
    private static final String SQL_SELECT_BY_LOGIN_AND_PASSWORD = "SELECT * FROM client" +
            " INNER JOIN role ON role.id = client.role_id WHERE login = ? AND password = md5(?)";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM client WHERE id = ?";
    private static final String SQL_UPDATE_BY_ID = "UPDATE client SET name=?, surname=?, login=?, is_blocked=?," +
            " phone_number=? WHERE id=?";
    private static final String SQL_SELECT_BY_LOGIN = "SELECT * FROM client WHERE login=?";

    private static final String ID = "id";

    public UserDaoImpl(Connection connection) {
        super(connection);
    }

    /**
     * Returns the user if exists in the database.
     * Can return NULL!
     *
     * @param login    A user login.
     * @param password A user password.
     * @return user if the pair login-password exists in the database
     * or return NULL otherwise.
     * @throws DaoException If PreparedStatement.setString throws SQLException
     * @see PreparedStatement
     */
    @Override
    public User findByLoginAndPassword(String login, String password) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_SELECT_BY_LOGIN_AND_PASSWORD);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);

            List<User> list = executeSelectQuery(preparedStatement);
            if (list.size() > 0) {
                LOGGER.debug("A user with a pair login-password has been found");
                return list.get(0);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        LOGGER.debug("A user with the pair login-password hasn't been found");
        return null;
    }

    /**
     * Selects and returns all users with specified role.
     *
     * @param role required user's role
     * @return ArrayList of users
     * @throws DaoException can be caused by SQLException
     */
    @Override
    public List<User> findUsersByRole(UserRole role) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_SELECT_BY_ROLE);

            String roleStr = role.toString();
            String roleLowerCase = roleStr.toLowerCase();
            preparedStatement.setString(1, roleLowerCase);

            List<User> list = executeSelectQuery(preparedStatement);

            if (list.size() > 0) {
                LOGGER.debug("A user(s) with a role " + roleLowerCase + " has been found.");
            } else {
                LOGGER.debug("A users with a role " + role + " haven't been found.");
            }

            return list;
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Selects and returns user id by login.
     * Returns null if does not exist.
     * @param login user login
     * @return Integer user id
     * @throws DaoException if SQLException
     */
    @Override
    public Integer findIdByLogin(String login) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_SELECT_BY_LOGIN);
            preparedStatement.setString(1, login);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(ID);
            }
            return null;
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    @Override
    protected User build(ResultSet resultSet) throws SQLException {
        String login = resultSet.getString("login");
        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");

        byte isBlocked = resultSet.getByte("is_blocked");
        String phoneNumber = resultSet.getString("phone_number");

        String role = resultSet.getString("role_name");
        String roleUpper = role.toUpperCase();
        UserRole userRole = UserRole.valueOf(roleUpper);

        Integer id = resultSet.getInt("client.id");

        return new User(id, name, surname, login, isBlocked, phoneNumber, userRole, null);
    }

    @Override
    protected PreparedStatement getSavePreparedStatement(User object) throws SQLException {

        UserRole userRole = object.getRole();
        String role = userRole.toString();
        String roleLowerCase = role.toLowerCase();

        Integer id = object.getEntityId();
        PreparedStatement preparedStatement;

        if (id != null) {
            preparedStatement = getPreparedStatement(SQL_UPDATE_BY_ID);
            preparedStatement.setInt(6, id);
        } else {
            preparedStatement = getPreparedStatement(SQL_INSERT_USER);
            preparedStatement.setString(6, roleLowerCase);

            String password = object.getPassword();
            preparedStatement.setString(7, password);
        }

        String name = object.getName();
        preparedStatement.setString(1, name);

        String surname = object.getSurname();
        preparedStatement.setString(2, surname);

        String login = object.getLogin();
        preparedStatement.setString(3, login);

        byte isBlocked = object.getIsBlocked() ? (byte) 1 : (byte) 0;
        preparedStatement.setByte(4, isBlocked);

        String phoneNumber = object.getPhoneNumber();
        preparedStatement.setString(5, phoneNumber);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getAllPreparedStatement() throws SQLException {
        return getPreparedStatement(SQL_SELECT_ALL_USERS);
    }

    @Override
    protected PreparedStatement getByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_SELECT_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getDeleteByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_DELETE_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }
}
