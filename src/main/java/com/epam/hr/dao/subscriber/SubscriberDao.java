package com.epam.hr.dao.subscriber;

import com.epam.hr.dao.Dao;
import com.epam.hr.dao.dto.SubscriptionDto;
import com.epam.hr.entity.subscriber.Subscriber;
import com.epam.hr.entity.subscriber.SubscriberStatus;
import com.epam.hr.exception.DaoException;

import java.util.List;

public interface SubscriberDao extends Dao<Subscriber> {

    /**
     * Returns subscriberStatus by userId and vacancyId.
     * Can return NULL.
     *
     * @param userId    user ID
     * @param vacancyId vacancy ID
     * @return subscriberStatus or NULL
     * @throws DaoException if SQLException
     */
    SubscriberStatus findSubscriberStatus(Integer userId, Integer vacancyId) throws DaoException;

    /**
     * Returns subscriberStatus by subscriberId.
     *
     * @param subscriberId Integer subscriber's Id
     * @return subscriberStatus or NULL
     * @throws DaoException if SQLException
     */
    SubscriberStatus findSubscriberStatus(Integer subscriberId) throws DaoException;

    /**
     * Returns subscriber ID if subscriber with parameters below exists in a database.
     * Otherwise returns null.
     *
     * @param userId    Integer user's Id
     * @param vacancyId Integer vacancy Id
     * @return ID if subscriber with userId and vacancyId exists in a database
     * @throws DaoException if SQLException
     */
    Integer findSubscriber(Integer userId, Integer vacancyId) throws DaoException;

    /**
     * Returns all active vacancies with subscriptions
     * searching by user ID.
     *
     * @param id user id
     * @return ArrayList of SubscriptionDto
     * @throws DaoException if SQLException
     */
    List<SubscriptionDto> findSubscriptionsByUserId(Integer id) throws DaoException;

    /**
     * Returns all active subscription
     * searching by vacancy ID.
     *
     * @param id vacancy id
     * @return ArrayList of SubscriptionDto
     * @throws DaoException if SQLException
     */
    List<SubscriptionDto> findSubscriptionsByVacancyId(Integer id) throws DaoException;

}
