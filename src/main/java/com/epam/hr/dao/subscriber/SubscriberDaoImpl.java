package com.epam.hr.dao.subscriber;

import com.epam.hr.dao.AbstractDao;
import com.epam.hr.dao.dto.SubscriptionDto;
import com.epam.hr.entity.subscriber.Subscriber;
import com.epam.hr.entity.subscriber.SubscriberStatus;
import com.epam.hr.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for a subscriber entity.
 *
 * @author Dolnikov Vladislav
 */
public class SubscriberDaoImpl extends AbstractDao<Subscriber> implements SubscriberDao {
    private static final Logger LOGGER = LogManager.getLogger(SubscriberDaoImpl.class);

    private static final String SQL_SELECT_ALL_SUBSCRIBERS = "SELECT * FROM subscriber";
    private static final String SQL_INSERT_SUBSCRIBER = "INSERT INTO subscriber(id," +
            "is_active, vacancy_id, status, client_id) VALUES (NULL,?,?,?,?)";
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM subscriber WHERE id = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM subscriber WHERE id = ?";
    private static final String SQL_UPDATE_BY_ID = "UPDATE subscriber SET " +
            "is_active=?, vacancy_id=?, status=?, client_id=? WHERE id=?";
    private static final String SQL_GET_STATUS = "SELECT subscriber.status" +
            " FROM subscriber INNER JOIN vacancy ON vacancy.id = subscriber.vacancy_id" +
            " WHERE subscriber.client_id=? AND vacancy.id=?";
    private static final String SQL_GET_STATUS_BY_ID = "SELECT status" +
            " FROM subscriber WHERE subscriber.id=?";
    private static final String SQL_SELECT_BY_USERID_AND_VACANCYID = "SELECT * FROM subscriber" +
            " WHERE client_id = ? AND vacancy_id=?";
    private static final String SQL_GET_SUBSCRIPTIONS_BY_USER_ID = "SELECT * FROM subscriber INNER JOIN" +
            " vacancy ON vacancy.id = subscriber.vacancy_id INNER JOIN client ON client.id=subscriber.client_id " +
            "WHERE vacancy.is_active=1 AND subscriber.is_active=1 AND client.is_blocked=0 AND subscriber.client_id=?";
    private static final String SQL_GET_SUBSCRIPTIONS_BY_VACANCY_ID = "SELECT * FROM subscriber INNER JOIN" +
            " vacancy ON vacancy.id = subscriber.vacancy_id INNER JOIN client ON client.id=subscriber.client_id " +
            "WHERE vacancy.is_active=1 AND subscriber.is_active=1 AND client.is_blocked=0 AND subscriber.vacancy_id =?";

    private static final byte IS_ACTIVE_SUBSCRIBER_DEFAULT = 1;
    private static final byte IS_BLOCKED_DEFAULT = 0;

    public SubscriberDaoImpl(Connection connection) {
        super(connection);
    }


    /**
     * Returns subscriberStatus by userId and vacancyId.
     * Can return NULL.
     *
     * @param userId    user ID
     * @param vacancyId vacancy ID
     * @return subscriberStatus or NULL
     * @throws DaoException if SQLException
     */
    @Override
    public SubscriberStatus findSubscriberStatus(Integer userId, Integer vacancyId) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_STATUS);

            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, vacancyId);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String status = resultSet.getString("status");
                String statusUpper = status.toUpperCase();
                return SubscriberStatus.valueOf(statusUpper);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Returns subscriberStatus by subscriberId.
     *
     * @param subscriberId Integer subscriber's Id
     * @return subscriberStatus or NULL
     * @throws DaoException if SQLException
     */
    @Override
    public SubscriberStatus findSubscriberStatus(Integer subscriberId) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_STATUS_BY_ID);

            preparedStatement.setInt(1, subscriberId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String status = resultSet.getString("status");
                String statusUpper = status.toUpperCase();
                return SubscriberStatus.valueOf(statusUpper);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Returns subscriber ID if subscriber with parameters below exists in a database.
     * Otherwise returns null.
     *
     * @param userId    Integer user's Id
     * @param vacancyId Integer vacancy Id
     * @return ID if subscriber with userId and vacancyId exists in a database
     * @throws DaoException if SQLException
     */
    @Override
    public Integer findSubscriber(Integer userId, Integer vacancyId) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_SELECT_BY_USERID_AND_VACANCYID);

            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, vacancyId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                LOGGER.debug("Subscriber has been found.");
                return resultSet.getInt("id");
            }

            LOGGER.debug("Subscriber hasn't been found. Return null from SubscrDao.");
            return null;
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Returns all active vacancies with subscriptions
     * searching by user ID.
     *
     * @return ArrayList of SubscriptionDto
     * @throws DaoException if SQLException
     */
    @Override
    public List<SubscriptionDto> findSubscriptionsByUserId(Integer id) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_SUBSCRIPTIONS_BY_USER_ID);
            return createSubscriptionsDtoList(id, preparedStatement);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }

    }

    /**
     * Returns all active subscription
     * searching by vacancy ID.
     *
     * @param id vacancy id
     * @return ArrayList of SubscriptionDto
     * @throws DaoException if SQLException
     */
    @Override
    public List<SubscriptionDto> findSubscriptionsByVacancyId(Integer id) throws DaoException {
        try {
            PreparedStatement preparedStatement = getPreparedStatement(SQL_GET_SUBSCRIPTIONS_BY_VACANCY_ID);
            return createSubscriptionsDtoList(id, preparedStatement);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    @Override
    protected PreparedStatement getSavePreparedStatement(Subscriber object) throws SQLException, DaoException {

        Integer id = object.getEntityId();
        PreparedStatement preparedStatement;

        if (id != null) {
            preparedStatement = getPreparedStatement(SQL_UPDATE_BY_ID);
            preparedStatement.setInt(5, id);
        } else {
            preparedStatement = getPreparedStatement(SQL_INSERT_SUBSCRIBER);
        }

        byte isActive = object.getIsActive();
        preparedStatement.setByte(1, isActive);

        Integer vacancyId = object.getVacancyId();
        preparedStatement.setInt(2, vacancyId);

        String statusLowerCase = statusToLowerCaseString(object);
        preparedStatement.setString(3, statusLowerCase);

        Integer userID = object.getUserId();
        preparedStatement.setInt(4, userID);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getAllPreparedStatement() throws SQLException {
        return getPreparedStatement(SQL_SELECT_ALL_SUBSCRIBERS);

    }

    @Override
    protected PreparedStatement getByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_SELECT_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }

    @Override
    protected PreparedStatement getDeleteByIdPreparedStatement(int id) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement(SQL_DELETE_BY_ID);
        preparedStatement.setInt(1, id);

        return preparedStatement;
    }

    @Override
    protected Subscriber build(ResultSet resultSet) throws SQLException {

        Integer id = resultSet.getInt("id");
        Integer vacancyId = resultSet.getInt("vacancy_id");
        Integer userId = resultSet.getInt("client_id");

        String statusString = resultSet.getString("status");
        String statusUpperCase = statusString.toUpperCase();
        SubscriberStatus status = SubscriberStatus.valueOf(statusUpperCase);

        byte isActive = resultSet.getByte("is_active");

        return new Subscriber(id, isActive, vacancyId, userId, status);
    }

    private String statusToLowerCaseString(Subscriber subscriber) throws DaoException {
        SubscriberStatus status = subscriber.getStatus();
        if (status != null) {
            String typeString = status.toString();
            return typeString.toLowerCase();
        }
        throw new DaoException("Field 'status' of the object 'subscriber' is null.");
    }

    private SubscriptionDto buildSubscriptionDto(ResultSet resultSet) throws SQLException {
        /*Vacancy*/
        String specialization = resultSet.getString("specialization");
        String location = resultSet.getString("location");
        String description = resultSet.getString("description");
        byte isActiveVacancy = resultSet.getByte("vacancy.is_active");
        Integer vacancyId = resultSet.getInt("vacancy.id");

        /*Subscriber*/
        Integer subscriberId = resultSet.getInt("subscriber.id");
        Integer userId = resultSet.getInt("client_id");
        String statusString = resultSet.getString("status");
        String statusUpperCase = statusString.toUpperCase();
        SubscriberStatus status = SubscriberStatus.valueOf(statusUpperCase);

        /*User*/
        String login = resultSet.getString("login");
        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");

        String phoneNumber = resultSet.getString("phone_number");

        return new SubscriptionDto(vacancyId, specialization,
                description, location, isActiveVacancy, subscriberId, status, IS_ACTIVE_SUBSCRIBER_DEFAULT,
                userId, login, name, surname, IS_BLOCKED_DEFAULT, phoneNumber);
    }

    private List<SubscriptionDto> createSubscriptionsDtoList(Integer id, PreparedStatement preparedStatement) throws SQLException {

        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<SubscriptionDto> list = new ArrayList<>();
        while (resultSet.next()) {
            SubscriptionDto subscriptionDto = buildSubscriptionDto(resultSet);
            list.add(subscriptionDto);
        }
        return list;
    }
}
