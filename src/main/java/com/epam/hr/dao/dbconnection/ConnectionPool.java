package com.epam.hr.dao.dbconnection;

import com.epam.hr.exception.ConnectionPoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Connection pool for operations with freeConnections.
 *
 * @author Dolnikov Vladislav
 */
public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    private static AtomicBoolean initialized = new AtomicBoolean(false);
    private static AtomicBoolean isActivePool = new AtomicBoolean(false);

    private static ConnectionPool instance;

    private static ReentrantLock instanceLock = new ReentrantLock();
    private static ReentrantLock connectionsQueueLock = new ReentrantLock();

    private final Semaphore semaphore;

    private Integer poolSize;
    private Queue<Connection> freeConnections;
    private Queue<Connection> allConnections;
    private ConnectionFactory connectionFactory;

    private ConnectionPool() {
        initPoolSize();
        connectionFactory = new ConnectionFactory();
        freeConnections = new LinkedList<>();
        allConnections = new LinkedList<>();
        initConnectionPool();
        semaphore = new Semaphore(poolSize, true);
        isActivePool.set(true);
    }

    /**
     * Returns instance of the connection pool.
     *
     * @return ConnectionPool
     */
    public static ConnectionPool getInstance() {
        if (!initialized.get()) {
            instanceLock.lock();
            try {
                if (!initialized.get()) {
                    instance = new ConnectionPool();
                    initialized.set(true);
                }
            } finally {
                instanceLock.unlock();
            }
        }
        return instance;
    }

    /**
     * Returns a free connection.
     *
     * @return free connection
     * @throws ConnectionPoolException if InterruptedException has been thrown.
     */
    public Connection getConnection() throws ConnectionPoolException {
        try {
            semaphore.acquire();
            if (isActivePool.get()) {
                return pollFreeConnection();
            }
            throw new ConnectionPoolException("ConnectionPool has been already closed.");
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e.getMessage(), e);
        }
    }

    /**
     * Adds active connection to the freeConnections
     * after using if pool is active, otherwise closes
     * connection.
     *
     * @param connection opened connection
     */
    public void releaseConnection(Connection connection) {
        try {
            connectionsQueueLock.lock();
            if (isActivePool.get()) {
                freeConnections.add(connection);
                LOGGER.debug("Connection has been return to the freeConnections queue. Size: " + freeConnections.size());
            } else {
                connection.close();
                LOGGER.debug("Connection has been closed.");
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            connectionsQueueLock.unlock();
            semaphore.release();
        }
    }

    /**
     * Closes pool freeConnections and finishes work with a database.
     */
    public void closePool() {
        try {
            connectionsQueueLock.lock();
            isActivePool.set(false);
            Iterator listIterator = freeConnections.iterator();

            boolean hasNextElement = listIterator.hasNext();
            while (hasNextElement) {
                Connection currentConnection = (Connection) listIterator.next();
                currentConnection.close();
                LOGGER.debug("Connection " + currentConnection.toString() + " has been closed.");
                hasNextElement = listIterator.hasNext();
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
        } finally {
            connectionsQueueLock.unlock();
        }

    }

    private Connection pollFreeConnection() throws ConnectionPoolException {
        try {
            connectionsQueueLock.lock();
            if (freeConnections.isEmpty()) {
                throw new ConnectionPoolException("Cannot get connection: freeConnections queue is empty.");
            } else {
                LOGGER.debug("A new connection has been obtained from the pool. Free connections in the pool: "
                        + (freeConnections.size() - 1));
                return freeConnections.poll();
            }
        } finally {
            connectionsQueueLock.unlock();
        }
    }

    private void initConnectionPool() {
        try {
            for (int i = 0; i < poolSize; i++) {
                Connection connection = connectionFactory.createNewConnection();
                freeConnections.add(connection);
                allConnections.add(connection);
            }
        } catch (ConnectionPoolException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void initPoolSize() {
        ResourceBundle dbProperties = ResourceBundle.getBundle(ConnectionFactory.DB_PROPS_FILE);
        String poolSizeString = dbProperties.getString("db.pool.size");
        poolSize = Integer.parseInt(poolSizeString);
    }
}
