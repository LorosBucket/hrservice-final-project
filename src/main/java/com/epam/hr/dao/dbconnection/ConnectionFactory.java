package com.epam.hr.dao.dbconnection;

import com.epam.hr.exception.ConnectionPoolException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Class for hiding a connection creation.
 *
 * @author Dolnikov Vladislav
 */
public class ConnectionFactory {
    /*package*/ static final String DB_PROPS_FILE = "database";

    private String url;
    private String user;
    private String pass;

    /*package*/ ConnectionFactory(){
        ResourceBundle dbProperties = ResourceBundle.getBundle(DB_PROPS_FILE);

        this.url = dbProperties.getString("db.url");
        this.user = dbProperties.getString("db.user");
        this.pass = dbProperties.getString("db.password");
    }

    /*package*/ Connection createNewConnection() throws ConnectionPoolException {
        try {
            return DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            throw new ConnectionPoolException(e.getMessage(), e);
        }
    }
}
