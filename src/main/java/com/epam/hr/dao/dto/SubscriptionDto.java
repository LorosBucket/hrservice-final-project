package com.epam.hr.dao.dto;

import com.epam.hr.entity.subscriber.SubscriberStatus;

/**
 * Data transfer object for a user's subscription.
 */
public class SubscriptionDto {
    /*Vacancy*/
    private Integer vacancyId;
    private String specialization;
    private String description;
    private String location;
    private byte isActiveVacancy;

    /*Subscriber*/
    private Integer subscriberId;
    private SubscriberStatus status;
    private byte isActiveSubscriber;
    private Integer userId;

    /*User*/
    private String login;
    private String name;
    private String surname;
    private byte isBlocked;
    private String phoneNumber;

    public SubscriptionDto(Integer vacancyId, String specialization, String description,
                           String location, byte isActiveVacancy, Integer subscriberId,
                           SubscriberStatus status, byte isActiveSubscriber, Integer userId,
                           String login, String name, String surname, byte isBlocked, String phoneNumber) {
        this.vacancyId = vacancyId;
        this.specialization = specialization;
        this.description = description;
        this.location = location;
        this.isActiveVacancy = isActiveVacancy;
        this.subscriberId = subscriberId;
        this.status = status;
        this.isActiveSubscriber = isActiveSubscriber;
        this.userId = userId;
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.isBlocked = isBlocked;
        this.phoneNumber = phoneNumber;
    }

    public Integer getVacancyId() {
        return vacancyId;
    }

    public String getSpecialization() {
        return specialization;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public byte getIsActiveVacancy() {
        return isActiveVacancy;
    }

    public Integer getSubscriberId() {
        return subscriberId;
    }

    public SubscriberStatus getStatus() {
        return status;
    }

    public byte getIsActiveSubscriber() {
        return isActiveSubscriber;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public byte getIsBlocked() {
        return isBlocked;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
