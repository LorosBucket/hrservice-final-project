package com.epam.hr.entity.vacancy;

import com.epam.hr.entity.AbstractEntity;
import java.io.Serializable;

/**
 * JavaBean for a vacancy. Contains vacancy specialization,
 * description, location and status(active/hidden).
 */
public class Vacancy extends AbstractEntity implements Serializable {
    private String specialization;
    private String description;
    private String location;
    private byte isActive;

    public Vacancy(Integer id, String specialization, String location,
                   String description, byte isActive) {
        super(id);
        this.specialization = specialization;
        this.location = location;
        this.description = description;
        this.isActive = isActive;
    }

    public String getSpecialization() {
        return specialization;
    }

    public String getDescription() {
        return description;
    }

    public byte getIsActive() {
        return isActive;
    }

    public String getLocation() {
        return location;
    }

    public void setIsActive(byte isActive) {
        this.isActive = isActive;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Vacancy vacancy = (Vacancy) obj;

        if (isActive != vacancy.isActive) return false;
        if (specialization != null ? !specialization.equals(vacancy.specialization) : vacancy.specialization != null)
            return false;
        if (description != null ? !description.equals(vacancy.description) : vacancy.description != null) return false;
        return location != null ? location.equals(vacancy.location) : vacancy.location == null;
    }

    @Override
    public int hashCode() {
        int result = specialization != null ? specialization.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (int) isActive;
        return result;
    }
}
