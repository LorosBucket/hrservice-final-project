package com.epam.hr.entity.subscriber;

/**
 * Status of a subscriber. Contains states:
 * submitted, interview appointed, job offer,
 * rejected.
 *
 * @author Dolnikov Vladislav
 */
public enum SubscriberStatus {
    SUBMITTED,
    INTERVIEW_APPOINTED,
    JOB_OFFER,
    REJECTED
}
