package com.epam.hr.entity.subscriber;

import com.epam.hr.entity.AbstractEntity;

import java.io.Serializable;

/**
 * JavaBean for a subscriber. Contains subscriber state(banned, active),
 * user ID, vacancy ID, subscriber status.
 *
 * @author Dolnikov Vladislav
 */
public class Subscriber extends AbstractEntity implements Serializable {
    private byte isActive;
    private Integer userId;
    private Integer vacancyId;
    private SubscriberStatus status;

    public Subscriber(Integer id, byte isActive, Integer vacancyId, Integer userId, SubscriberStatus status) {
        super(id);
        this.isActive = isActive;
        this.vacancyId = vacancyId;
        this.userId = userId;
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public SubscriberStatus getStatus() {
        return status;
    }

    public Integer getVacancyId() {
        return vacancyId;
    }

    public byte getIsActive() {
        return isActive;
    }

    public void setIsActive(byte isActive) {
        this.isActive = isActive;
    }

    public void setStatus(SubscriberStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Subscriber that = (Subscriber) obj;

        if (isActive != that.isActive) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (vacancyId != null ? !vacancyId.equals(that.vacancyId) : that.vacancyId != null) return false;
        return status == that.status;
    }

    @Override
    public int hashCode() {
        int result = (int) isActive;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (vacancyId != null ? vacancyId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
