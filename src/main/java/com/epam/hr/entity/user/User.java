package com.epam.hr.entity.user;

import com.epam.hr.entity.AbstractEntity;

import java.io.Serializable;

/**
 * JavaBean for a user. Contains user login, user name,
 * user surname, user state(banned, active), user phone number,
 * user role and user password(only if necessary).
 *
 * @author Dolnikov Vladislav
 */
public class User extends AbstractEntity implements Serializable {
    private String login;
    private String name;
    private String surname;
    private byte isBlocked;
    private String phoneNumber;
    private UserRole role;
    private String password;

    public User(Integer id, String name, String surname,
                String login, byte isBlocked, String phoneNumber, UserRole role, String password){
        super(id);
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.isBlocked = isBlocked;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public boolean getIsBlocked() {
        return isBlocked == 1;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public UserRole getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setIsBlocked(byte isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        User user = (User) obj;

        if (isBlocked != user.isBlocked) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (surname != null ? !surname.equals(user.surname) : user.surname != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(user.phoneNumber) : user.phoneNumber != null) return false;
        if (role != user.role) return false;
        return password != null ? password.equals(user.password) : user.password == null;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (int) isBlocked;
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
