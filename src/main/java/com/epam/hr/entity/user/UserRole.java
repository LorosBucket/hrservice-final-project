package com.epam.hr.entity.user;

/**
 * Role of a user. Contains roles: admin, hr, aspirant.
 *
 * @author Dolnikov Vladislav
 */
public enum UserRole {
    ADMIN,
    HR,
    ASPIRANT
}
