package com.epam.hr.entity.interview;

/**
 * Status of interview. Contains states:
 * rejected, passed, appointed.
 *
 * @author Dolnikov Vladislav
 */
public enum InterviewStatus {
    REJECTED,
    PASSED,
    APPOINTED
}
