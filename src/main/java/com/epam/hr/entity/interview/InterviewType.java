package com.epam.hr.entity.interview;

/**
 * Type of interview. Contains states:
 * technical, introductory, behavioral,
 * case, group.
 */
public enum InterviewType {
    TECHNICAL,
    INTRODUCTORY,
    BEHAVIORAL,
    CASE,
    GROUP
}
