package com.epam.hr.entity.interview;

import com.epam.hr.entity.AbstractEntity;

import java.io.Serializable;

/**
 * JavaBean for Interview. Contains type of an interview, interview status,
 * comment and subscriber ID.
 *
 * @author Dolnikov Vladislav
 */
public class Interview extends AbstractEntity implements Serializable {
    private InterviewType type;
    private InterviewStatus status;
    private String comment;
    private Integer subscriberId;

    public Interview(Integer id, InterviewType type,
                     InterviewStatus status, Integer subscriberId) {
        super(id);
        this.type = type;
        this.status = status;
        this.subscriberId = subscriberId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public InterviewType getType() {
        return type;
    }

    public InterviewStatus getStatus() {
        return status;
    }

    public String getComment() {
        return comment;
    }

    public Integer getSubscriberId() {
        return subscriberId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Interview interview = (Interview) obj;

        if (type != interview.type) return false;
        if (status != interview.status) return false;

        if (comment == null && interview.comment != null) {
            return false;
        }
        if (comment != null && !comment.equals(interview.comment)) {
            return false;
        }
        return subscriberId != null ? subscriberId.equals(interview.subscriberId) : interview.subscriberId == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (subscriberId != null ? subscriberId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Interview{" +
                "id=" + getEntityId() +
                ", type=" + type +
                ", status=" + status +
                ", comment='" + comment + '\'' +
                ", subscriberId=" + subscriberId +
                '}';
    }
}
