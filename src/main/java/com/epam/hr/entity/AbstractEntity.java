package com.epam.hr.entity;

import java.io.Serializable;

/**
 * Parent superclass of all JavaBeans.
 * Contains entity ID.
 *
 * @author Dolnikov Vladislav
 */
public abstract class AbstractEntity implements Serializable {
    private Integer entityId;

    public AbstractEntity(Integer entityId) {
        this.entityId = entityId;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        AbstractEntity that = (AbstractEntity) obj;

        return entityId != null ? entityId.equals(that.entityId) : that.entityId == null;
    }

    @Override
    public int hashCode() {
        return entityId != null ? entityId.hashCode() : 0;
    }
}
