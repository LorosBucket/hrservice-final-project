package com.epam.hr.exception;

/**
 * Handles connection pool exceptions.
 *
 * @author Dolnikov Vladislav
 */
public class ConnectionPoolException extends HrApplicationException {

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionPoolException(String message) {
        super(message);
    }
}