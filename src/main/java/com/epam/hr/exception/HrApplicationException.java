package com.epam.hr.exception;

/**
 * Handles application exceptions. Parent class for
 * all application exceptions.
 *
 * @author Dolnikov Vladislav
 */
public class HrApplicationException extends Exception {
    public HrApplicationException() {

    }

    public HrApplicationException(String message) {
        super(message);
    }

    public HrApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
