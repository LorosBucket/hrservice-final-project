package com.epam.hr.exception;

/**
 * Handles command layer exceptions
 *
 * @author Dolnikov Vladislav
 */
public class CommandException extends HrApplicationException {

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }
}
