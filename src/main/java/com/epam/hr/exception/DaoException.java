package com.epam.hr.exception;

/**
 * Handles Dao exceptions.
 *
 * @author Dolnikov Vladislav
 */
public class DaoException extends HrApplicationException {
    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
