package com.epam.hr.exception;

/**
 * Handles exceptions of the service layer.
 *
 * @author Dolnikov Vladislav
 */
public class ServiceException extends HrApplicationException {
    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
