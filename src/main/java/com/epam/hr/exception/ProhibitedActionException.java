package com.epam.hr.exception;

/**
 * Handles exceptions of prohibited actions in the client(illegal action for user role).
 * Arises in the page security filter.
 *
 * @author Dolnikov Vladislav
 */
public class ProhibitedActionException extends HrApplicationException {
    public ProhibitedActionException() {

    }
}
