package com.epam.hr.util;

import java.util.ResourceBundle;

public enum Pages {
    INDEX("path.page.index"),
    LOGIN("path.page.login"),

    ERROR("path.page.error"),
    SERVER_ERROR("path.page.server.error"),

    ASPIRANT_HOME("path.page.aspirant.home"),
    ASPIRANT_SUBSCRIPTIONS("path.page.aspirant.subscriptions"),

    HR_HOME("path.page.hr.home"),
    VACANCY_SUBSCRIBERS("path.page.hr.vacancy.subscribers"),

    ASPIRANTS_LIST("path.page.admin.aspirants.list"),
    HRS_LIST("path.page.admin.hrs.list"),

    VACANCIES_LIST("path.page.vacancies.list"),
    SUBSCRIBER_CONTROL("path.page.subscriber.control");

    public static final String CONFIG = "config";
    private String url;

    ResourceBundle resourceBundle = ResourceBundle.getBundle(CONFIG);

    Pages(String key) {
        this.url = resourceBundle.getString(key);
    }

    public String getUrl() {
        return url;
    }
}
