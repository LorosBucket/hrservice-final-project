package com.epam.hr.util.validator;

/**
 * Class-validator for a login.
 */
public class LoginValidator {
    private static final int MIN_LOGIN_SIZE = 2;
    private static final int MAX_LOGIN_SIZE = 20;

    /**
     * Validates login for a empty string,
     * whitespaces and size.
     *
     * @param login entered login
     * @return true if valid login,
     * otherwise false.
     */
    public static boolean validate(String login) {
        if (!ValidatorUtils.validateOnEmptyString(login)) {
            //Login is empty.
            return false;
        }
        if (!ValidatorUtils.validateOnWhitespacesString(login)) {
            //Login can not contain whitespaces.
            return false;
        }
        if (!ValidatorUtils.validateStringSize(login, MIN_LOGIN_SIZE, MAX_LOGIN_SIZE)) {
            //Login size must be from MIN_LOGIN_SIZE to MAX_LOGIN_SIZE
            return false;
        }
        return true;
    }
}
