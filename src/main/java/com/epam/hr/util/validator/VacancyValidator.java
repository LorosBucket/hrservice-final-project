package com.epam.hr.util.validator;

/**
 * Class-validator for a vacancy.
 */
public class VacancyValidator {
    private static final int MIN_SPEC_SIZE = 1;
    private static final int MIN_LOC_SIZE = 1;
    private static final int MIN_DESC_SIZE = 1;

    private static final int MAX_SPEC_SIZE = 100;
    private static final int MAX_LOC_SIZE = 100;
    private static final int MAX_DESC_SIZE = 1000;

    private static final int DEFAULT_HIDDEN = 0;
    private static final int DEFAULT_ACTIVE = 1;

    /**
     * Validates vacancy for a emtry fields
     * and fields sizes.
     *
     * @param spec   vacancy specialization
     * @param loc    vacancy location
     * @param desc   vacancy description
     * @param status vacancy status(active/hidden)
     * @return true if valid, otherwise false.
     */
    public static boolean validate(String spec, String loc, String desc, byte status) {
        if (!ValidatorUtils.validateOnEmptyString(spec) ||
                !ValidatorUtils.validateOnEmptyString(loc) ||
                !ValidatorUtils.validateOnEmptyString(desc)) {
            return false;
        }

        if (status != DEFAULT_HIDDEN && status != DEFAULT_ACTIVE) {
            return false;
        }

        if (!ValidatorUtils.validateStringSize(spec, MIN_SPEC_SIZE, MAX_SPEC_SIZE) ||
                !ValidatorUtils.validateStringSize(desc, MIN_DESC_SIZE, MAX_DESC_SIZE) ||
                !ValidatorUtils.validateStringSize(loc, MIN_LOC_SIZE, MAX_LOC_SIZE)) {
            return false;
        }

        return true;
    }
}
