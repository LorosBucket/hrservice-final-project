package com.epam.hr.util.validator;

/**
 * Class-validator for a password.
 */
public class PasswordValidator {
    private static final int MIN_PASSWORD_SIZE = 4;
    private static final int MAX_PASSWORD_SIZE = 50;

    /**
     * Validates password for a empty string,
     * whitespaces and string size.
     *
     * @param password entered password
     * @return true if valid, otherwise false.
     */
    public static boolean validate(String password) {
        if (!ValidatorUtils.validateOnEmptyString(password)) {
            //Password is empty.
            return false;
        }
        if (!ValidatorUtils.validateOnWhitespacesString(password)) {
            //Password can not contain whitespaces.
            return false;
        }
        if (!ValidatorUtils.validateStringSize(password, MIN_PASSWORD_SIZE, MAX_PASSWORD_SIZE)) {
            //Password size must be from MIN_PASSWORD_SIZE to MAX_PASSWORD_SIZE
            return false;
        }
        return true;
    }
}
