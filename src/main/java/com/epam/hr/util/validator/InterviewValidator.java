package com.epam.hr.util.validator;

/**
 * Validator for private interview notes.
 * These notes can consist of all symbols.
 * Length from 1 to 1000 symbols.
 */
public class InterviewValidator {
    private static final int MIN_SIZE = 1;
    private static final int MAX_SIZE = 1000;

    /**
     * Validate note for a subscriber interview.
     * Return true if valid, otherwise false.
     * @param comment is a note content
     * @return true if valid, otherwise false
     */
    public static boolean validate(String comment) {
        if (!ValidatorUtils.validateOnEmptyString(comment)) {
            return false;
        }

        if (!ValidatorUtils.validateStringSize(comment, MIN_SIZE, MAX_SIZE)) {
            return false;
        }
        return true;
    }
}
