package com.epam.hr.util.validator;

/**
 * Class with methods for a data validation.
 */
public class ValidatorUtils {

    /**
     * Validate strings for a null size.
     *
     * @param str entered string
     * @return true if valid
     */
    public static boolean validateOnEmptyString(String... str) {
        for (String currentParam : str) {
            if (currentParam.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate string for a null size.
     *
     * @param str entered string
     * @return true if valid
     */
    public static boolean validateOnEmptyString(String str) {
        return str != null && !str.isEmpty();
    }

    /**
     * Validates string for a min and max sizes.
     *
     * @param str entered string
     * @param minSize min string size
     * @param maxSize max string size
     * @return true if string size larger min and less max.
     */
    public static boolean validateStringSize(String str, int minSize, int maxSize) {
        int strSize = str.length();
        return strSize >= minSize && strSize <= maxSize;
    }

    /**
     * Validates string for a whitespaces including.
     *
     * @param str entered string
     * @return true if does not contain whitespaces
     */
    public static boolean validateOnWhitespacesString(String str) {
        // 32 is a space character code
        char whitespace = 32;
        return str.indexOf(whitespace) < 0;
    }

    /**
     * Validates entered parameters for a null.
     *
     * @param param entered parameters
     * @return true if all not null parameters.
     */
    public static boolean checkParamsForNull(Object... param) {
        for (Object currentParam : param) {
            if (currentParam == null) {
                return false;
            }
        }
        return true;
    }
}
