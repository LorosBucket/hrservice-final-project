package com.epam.hr.util.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class-validator for a phone number.
 */
public class PhoneFormatValidator {

    private static final String REGEXP = "^\\+[0-9]{12}";

    /**
     * Validates an entered phone number for a template.
     *
     * @param phone entered phone number
     * @return true if valid, otherwise false.
     */
    public static boolean validate(String phone) {
        Pattern pattern = Pattern.compile(REGEXP);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }
}
