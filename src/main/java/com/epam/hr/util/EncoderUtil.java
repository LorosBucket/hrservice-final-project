package com.epam.hr.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.UnsupportedEncodingException;

public class EncoderUtil {
    private static final Logger LOGGER = LogManager.getLogger(EncoderUtil.class);

    /**
     * Encodes string from ISO-8859-1 to UTF-8
     *
     * @param string to encode
     * @return UTF-8 string
     */
    public static String encodeToUtf8(String string) {
        try {
            return new String(string.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return string;
    }
}
