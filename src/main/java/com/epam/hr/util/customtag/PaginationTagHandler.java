package com.epam.hr.util.customtag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Class-handler for a custom jsp tag.
 * Gets page index, rows amount, counter and current page command.
 * Creates and outputs html code for a pagination.
 *
 * @author Dolnikov Vladislav
 */
public class PaginationTagHandler extends TagSupport {
    private static final int DEFAULT_PAGE_INDEX = 1;
    private static final int DEFAULT_COUNT = 7;

    private static final String STYLE = "<!--pagination style-->\n<link href=\"/hr/css/components/pagination.css\"" +
            " rel=\"stylesheet\" type=\"text/css\"/>";
    private static final int WIDTH = 2;

    private String command;
    private Integer pageIndex;
    private Integer rows;
    private Integer count;

    public void setCommand(String command) {
        this.command = command;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public int doStartTag() throws JspException {

        if (pageIndex == null || pageIndex < 1) {
            pageIndex = DEFAULT_PAGE_INDEX;
        }

        if (count == null || count == 0) {
            count = DEFAULT_COUNT;
        }

        int pagesAmount = pagesAmount();

        if (pagesAmount < 2) {
            return SKIP_BODY;
        }

        int leftPageIndex = defineLeftIndex();
        int rightPageIndex = defineRightIndex(pagesAmount);

        try {
            outStyle();
            outCurrentRows();
            outPagination(leftPageIndex, rightPageIndex, pagesAmount);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    private void outCurrentRows() throws IOException {
        Integer firstRow = count * (pageIndex - 1) + 1;
        Integer lastRow = count * pageIndex;
        if (lastRow > rows) {
            lastRow = rows;
        }
        pageContext.getOut().write("<div id='current_rows'>" +
                firstRow + " - " + lastRow + " (" + rows + ")</div>");
    }

    private void outPagination(int leftPageIndex, int rightPageIndex, int pagesAmount) throws IOException {
        String openDivTag = "<div class='pagination'>";
        String closeDivTag = "</div>";
        StringBuilder pagingHtmlCode = new StringBuilder();

        pagingHtmlCode.append(openDivTag);

        if (pageIndex > 1) {
            pagingHtmlCode
                    .append("<a href='")
                    .append(command)
                    .append("&")
                    .append("pageIndex=")
                    .append(pageIndex - 1)
                    .append("&count=")
                    .append(count)
                    .append("'>&laquo</a>");
        } else {
            pagingHtmlCode.append("<span>&laquo</span>");
        }

        for (int i = leftPageIndex; i <= rightPageIndex; i++) {
            pagingHtmlCode.append("<a ");

            if (i == pageIndex) {
                pagingHtmlCode.append("id='active_page' ");
            }
            pagingHtmlCode
                    .append("href='")
                    .append(command)
                    .append("&")
                    .append("pageIndex=")
                    .append(i)
                    .append("&count=")
                    .append(count)
                    .append("'>")
                    .append(i)
                    .append("</a>");
        }

        if (pageIndex < pagesAmount) {
            pagingHtmlCode.append("<a href='")
                    .append(command)
                    .append("&")
                    .append("pageIndex=")
                    .append(pageIndex + 1)
                    .append("&count=")
                    .append(count)
                    .append("'>&raquo</a>");
        } else {
            pagingHtmlCode.append("<span>&raquo</span>");
        }

        pagingHtmlCode.append(closeDivTag);
        pageContext.getOut().write(pagingHtmlCode.toString());
    }

    private void outStyle() throws IOException {
        pageContext.getOut().write(STYLE);
    }

    private int defineRightIndex(int pagesAmount) {
        int rightPageIndex = pageIndex + WIDTH;
        if (rightPageIndex <= pagesAmount) {
            return rightPageIndex;
        }
        return pagesAmount;
    }

    private int defineLeftIndex() {
        int leftPageIndex = pageIndex - WIDTH;
        if (leftPageIndex > 0) {
            return leftPageIndex;
        }
        /*First command index*/
        return 1;
    }

    private int pagesAmount() {
        return (int) Math.ceil((double) rows / (double) count);
    }
}
