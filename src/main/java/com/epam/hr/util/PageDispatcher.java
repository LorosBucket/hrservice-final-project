package com.epam.hr.util;

import com.epam.hr.entity.user.UserRole;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redirects to a home page after user authorization.
 * A home page is selected by the user role.
 *
 * @author Dolnikov Vladislav
 */
public class PageDispatcher {

    private static final String COMMAND_SHOW_VACANCIES_WITH_RESPONSES =
            "/controller?command=show_vacancies_with_responses";
    private static final String COMMAND_SHOW_VACANCIES = "/controller?command=show_vacancies";

    /**
     * Redirects to a home page after user authorization.
     * A home page is selected by the user role.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param role UserRole
     * @throws IOException
     */
    public static void redirectToHomePage(HttpServletRequest request, HttpServletResponse response, UserRole role)
            throws IOException {
        String pageUrl = request.getContextPath();
        switch (role) {
            case ASPIRANT: {
                pageUrl += COMMAND_SHOW_VACANCIES;
                break;
            }
            case HR: {
                pageUrl += COMMAND_SHOW_VACANCIES_WITH_RESPONSES;
                break;
            }
            case ADMIN: {
                pageUrl += COMMAND_SHOW_VACANCIES;
                break;
            }
        }
        response.sendRedirect(pageUrl);
    }

    public static void doForward(HttpServletRequest request, HttpServletResponse response, String page)
            throws IOException, ServletException {
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }
}
