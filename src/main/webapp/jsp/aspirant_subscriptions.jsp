<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">

    <!-- styles -->
    <link rel="icon" href="../favicon.ico"/>
    <link href="${pageContext.request.contextPath}/css/vacancies_list.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/components/buttons_style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/footer.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/header_panel.css" rel="stylesheet">

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <%--subscription statuses--%>
    <fmt:message bundle="${local}" key="local.status.submitted" var="submitted"/>
    <fmt:message bundle="${local}" key="local.status.interview.appointed" var="interview_appointed"/>
    <fmt:message bundle="${local}" key="local.status.job.offer" var="job_offer"/>
    <fmt:message bundle="${local}" key="local.status.rejected" var="rejected"/>

    <fmt:message bundle="${local}" key="local.title.applications" var="header_title"/>
    <fmt:message bundle="${local}" key="local.header.applications.before" var="applic_before"/>
    <fmt:message bundle="${local}" key="local.header.applications.after" var="applic_after"/>
    <fmt:message bundle="${local}" key="local.button.cancel" var="cancel"/>

    <fmt:message bundle="${local}" key="local.footer.contact" var="contact"/>
    <fmt:message bundle="${local}" key="local.footer.copyright" var="copyright"/>

    <title>${header_title}</title>

</head>

<body>
<div id="wrapper">

    <%--Navigation bar--%>
    <jsp:include page="components/navigation_bar.jsp"/>

    <div class="header-image">
        <img src="${pageContext.request.contextPath}/img/cabinet_header.jpg" alt="header_vacancies_list.jpg"/>
    </div>

    <div class="vac-list-div">

        <div class="header-panel">
            <h2>${header_title}</h2>
            <br>

            <h4>${applic_before}
                <c:if test="${not empty requestScope.subscriptionsDtoList}">
                    ${requestScope.dtoListSize}
                </c:if>
                <c:if test="${empty requestScope.subscriptionsDtoList}">0</c:if>
                ${applic_after}
            </h4>

        </div>


        <c:if test="${not empty requestScope.subscriptionsDtoList}">
            <table class="vacancies-table">
                <tbody>
                <c:forEach var="vacancy" items="${requestScope.subscriptionsDtoList}">
                    <tr>
                        <td class="spec-loc-td"><span>${vacancy.specialization}</span><br><br>${vacancy.location}</td>
                        <td class="descr-td">${vacancy.description}</td>
                        <td class="status-td">
                            <c:choose>
                                <c:when test="${vacancy.status eq 'SUBMITTED'}">
                                    ${submitted}
                                </c:when>
                                <c:when test="${vacancy.status eq 'INTERVIEW_APPOINTED'}">
                                    ${interview_appointed}
                                </c:when>
                                <c:when test="${vacancy.status eq 'JOB_OFFER'}">
                                    ${job_offer}
                                </c:when>
                                <c:when test="${vacancy.status eq 'REJECTED'}">
                                    ${rejected}
                                </c:when>
                            </c:choose>

                        </td>
                        <td class="actions-td">
                            <form name="HideSubscriberForm" method="POST"
                                  action="${pageContext.request.contextPath}/controller">
                                <input type="hidden" name="command" value="hide_subscriber"/>
                                <input type="hidden" name="vacancyId" value="${vacancy.vacancyId}"/>
                                <input type="submit" value="${cancel}" class="buttons" id="cancel_response"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>
    </div>

    <%--footer--%>
    <c:import url="components/footer.jsp"/>

    <%--wrapper--%>
</div>

</body>
</html>