<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>
<head>

    <meta charset="UTF-8">

    <%--styles--%>
    <link href="${pageContext.request.contextPath}/css/components/footer.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/users_table_style.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/components/buttons_style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/header_panel.css" rel="stylesheet">

    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <%--subscription statuses--%>
    <fmt:message bundle="${local}" key="local.title.hrs" var="hrs"/>
    <fmt:message bundle="${local}" key="local.name" var="label_name"/>
    <fmt:message bundle="${local}" key="local.surname" var="surname"/>
    <fmt:message bundle="${local}" key="local.phone.number" var="phone"/>
    <fmt:message bundle="${local}" key="local.status" var="status"/>
    <fmt:message bundle="${local}" key="local.actions" var="actions"/>
    <fmt:message bundle="${local}" key="local.block" var="block"/>
    <fmt:message bundle="${local}" key="local.unblock" var="unblock"/>
    <fmt:message bundle="${local}" key="local.login" var="label_login"/>
    <fmt:message bundle="${local}" key="local.message.empty.list" var="empty_list_msg"/>

    <title>${hrs}</title>

</head>

<body>
<div id="wrapper">

    <%--Navigation bar--%>
    <jsp:include page="components/navigation_bar.jsp"/>

    <div class="header-image">
        <img src="${pageContext.request.contextPath}/img/header_hrs.jpeg" alt="header.jpg"/>
    </div>

    <div class="page">

        <div class="header-panel">
            <h2>${hrs}</h2>

            <c:if test="${empty requestScope.hrsList}">
                <h2>${empty_list_msg}</h2>
            </c:if>
        </div>

        <c:if test="${not empty requestScope.hrsList}">

            <table class="users-table">
                <thead>
                <tr>
                    <th class="login-field">${label_login}</th>
                    <th class="name-field">${label_name} ${surname}</th>
                    <th class="phone-field">${phone}</th>
                    <th class="status-field">${status}</th>
                    <th class="actions-td">${actions}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="hr" items="${requestScope.hrsList}">
                    <tr>
                        <td class="login-field">${hr.login}</td>

                        <td class="name-field">${hr.name} ${hr.surname}</td>

                        <td class="phone-field">${hr.phoneNumber}</td>

                        <td class="status-field">

                            <c:choose>
                                <c:when test="${hr.isBlocked == true}">
                                    <img src="${pageContext.request.contextPath}/img/false.png" alt="blocked" width="30" height="30">
                                </c:when>
                                <c:when test="${hr.isBlocked == false}">
                                    <img src="${pageContext.request.contextPath}/img/true.png" alt="unblocked" width="30" height="30">
                                </c:when>
                            </c:choose>

                        </td>

                        <td class="actions-td">
                            <c:choose>
                                <c:when test="${hr.isBlocked == true}">
                                    <form name="UnblockAccountForm" method="POST"
                                          action="${pageContext.request.contextPath}/controller">
                                        <input type="hidden" name="command" value="unblock_account"/>
                                        <input type="hidden" name="userId" value="${hr.entityId}"/>
                                        <input type="hidden" name="commandPage" value="controller?command=show_hrs_list">
                                        <input type="submit" value="${unblock}" class="buttons" id="unblock_btn"/>
                                    </form>
                                </c:when>
                                <c:when test="${hr.isBlocked == false}">
                                    <form name="BlockAccountForm" method="POST"
                                          action="${pageContext.request.contextPath}/controller">
                                        <input type="hidden" name="command" value="block_account"/>
                                        <input type="hidden" name="userId" value="${hr.entityId}"/>
                                        <input type="hidden" name="commandPage" value="controller?command=show_hrs_list">
                                        <input type="submit" value="${block}" class="buttons" id="block_btn"/>
                                    </form>
                                </c:when>
                            </c:choose>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>

    </div>

    <%--footer--%>
    <c:import url="components/footer.jsp"/>

    <%--wrapper--%>
</div>
</body>
</html>