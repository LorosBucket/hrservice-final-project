<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">

    <%--styles--%>
    <link href="${pageContext.request.contextPath}/css/components/buttons_style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/vacancies_list.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/hr_home_page.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/components/footer.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/header_panel.css" rel="stylesheet">

    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>

    <c:set var="page" value="jsp/hr_home_page.jsp" scope="session"/>

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <fmt:message bundle="${local}" key="local.button.vacancy.aspirants" var="aspirants_btn"/>
    <fmt:message bundle="${local}" key="local.title.hr.home" var="title"/>
    <fmt:message bundle="${local}" key="local.header.hr.home" var="hr_home_header"/>
    <fmt:message bundle="${local}" key="local.message.empty.list" var="empty_list_msg"/>

    <title>${title}</title>

</head>
<body>
<div id="wrapper">


    <%--Navigation bar--%>
    <jsp:include page="components/navigation_bar.jsp"/>

    <%--<jsp:include command="${requestScope.contextPath}/controller">
        <jsp:param name="command" value="get_vacancies_with_responses"/>
    </jsp:include>--%>

    <div class="header-image">
        <img src="${pageContext.request.contextPath}/img/header_hr_home.jpeg" alt="header.jpg"/>
    </div>

    <div class="vac-list-div">

        <div class="header-panel">
            <h2>${title}</h2>

            <c:if test="${not empty requestScope.vacanciesList}">
                <h2>${hr_home_header}</h2>
            </c:if>

            <c:if test="${empty requestScope.vacanciesList}">
                <h2>${empty_list_msg}</h2>
            </c:if>
        </div>

        <c:if test="${not empty requestScope.vacanciesList}">

            <table class="vacancies-table">
                <tbody>
                <c:forEach var="vacancy" items="${requestScope.vacanciesList}">
                    <tr>
                        <td class="spec-loc-td"><span>${vacancy.specialization}</span><br><br>${vacancy.location}</td>
                        <td class="descr-td">${vacancy.description}</td>
                        <td class="actions-td">
                                <%--This form should put aspirantsList in request and redirect to the aspirants_list.jsp--%>
                            <form name="showAspirantsForm" action="${pageContext.request.contextPath}/controller">
                                <input type="hidden" name="command" value="show_vacancy_subscribers"/>
                                <input type="hidden" name="vacancyId" value="${vacancy.entityId}"/>
                                <input type="submit" value="${aspirants_btn}" class="buttons" id="show_aspirants_btn"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>

    </div>

    <%--footer--%>
    <c:import url="components/footer.jsp"/>

    <%--wrapper--%>
</div>

</body>

</html>