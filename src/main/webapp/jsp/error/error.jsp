<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>

<html>
<head>
    <title>Error 404 (Not Found)</title>

    <link href="${pageContext.request.contextPath}/css/error.css" rel="stylesheet">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>

</head>
<body>

<div id="container">

    <div class="error_msg">
        <span id="code">404</span>

        <span id="err">Error!<br><span id="err_comm">Page Not Found</span></span>
    </div>

    <div class="comment">
        <p>For Some Reason The Page You Requested Could Not Be Found On Our Server</p>
    </div>

    <span id="home">
        <c:choose>
            <c:when test="${sessionScope.user.role eq 'ASPIRANT'}">
                <a href="${pageContext.request.contextPath}/controller?command=show_aspirant_vacancies">Go Home</a>
            </c:when>

            <c:when test="${sessionScope.user.role eq 'HR'}">
                <a href="${pageContext.request.contextPath}/controller?command=show_vacancies_with_responses">Go Home</a>
            </c:when>

            <c:when test="${sessionScope.user.role eq 'ADMIN'}">
                <a href="${pageContext.request.contextPath}/controller?command=show_vacancies">Go Home</a>
            </c:when>

            <c:otherwise>
                <a href="${pageContext.request.contextPath}/index.jsp">Main page</a>
            </c:otherwise>
        </c:choose>
    </span>
</div>
</body>
</html>