<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>

<html>
<head>
    <title>Error 500 (Server Error)</title>

    <link href="${pageContext.request.contextPath}/css/internal_server_error.css" rel="stylesheet">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>

</head>
<body>

<div id="container">

    <div class="comment">
        <p>Whoops, looks like something went wrong...</p>
    </div>

    <div class="error_msg">
        <span id="code">500</span>

        <span id="err">Server Error!</span>
    </div>

    <span id="home">
        <c:choose>
            <c:when test="${sessionScope.user.role eq 'ASPIRANT'}">
                <a href="${pageContext.request.contextPath}/jsp/aspirant_home_page.jsp">Go Home</a>
            </c:when>

            <c:when test="${sessionScope.user.role eq 'HR'}">
                <a href="${pageContext.request.contextPath}/jsp/hr_home_page.jsp">Go Home</a>
            </c:when>

            <c:when test="${sessionScope.user.role eq 'ADMIN'}">
                <a href="${pageContext.request.contextPath}/jsp/admin_home_page.jsp">Go Home</a>
            </c:when>

            <c:otherwise>
                <a href="${pageContext.request.contextPath}/jsp/index.jsp">Main page</a>
            </c:otherwise>
        </c:choose>
    </span>

</div>
</body>
</html>