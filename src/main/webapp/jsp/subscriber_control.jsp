<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">

    <!-- styles post -->
    <link href="${pageContext.request.contextPath}/css/components/style-form.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/buttons_style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/err_messages.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/subscriber_control.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/components/footer.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/header_panel.css" rel="stylesheet">

    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>

    <c:set var="page" value="jsp/subscriber_control.jsp" scope="session"/>

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <fmt:message bundle="${local}" key="local.name" var="label_name"/>
    <fmt:message bundle="${local}" key="local.surname" var="surname"/>
    <fmt:message bundle="${local}" key="local.phone.number" var="phone"/>
    <fmt:message bundle="${local}" key="local.status" var="status"/>
    <fmt:message bundle="${local}" key="local.change.status" var="change_status"/>
    <fmt:message bundle="${local}" key="local.login" var="label_login"/>
    <fmt:message bundle="${local}" key="local.button.submit" var="submit"/>
    <fmt:message bundle="${local}" key="local.button.add.note" var="add_note_btn"/>
    <fmt:message bundle="${local}" key="local.button.delete" var="delete"/>

    <fmt:message bundle="${local}" key="local.title.subscriber.control" var="subscriber_control_title"/>
    <fmt:message bundle="${local}" key="local.title.new.note" var="new_note_title"/>
    <fmt:message bundle="${local}" key="local.title.private.note" var="private_note_title"/>
    <fmt:message bundle="${local}" key="local.title.interview.type" var="interview_type_title"/>
    <fmt:message bundle="${local}" key="local.title.interview.status" var="interview_status_title"/>

    <fmt:message bundle="${local}" key="local.interview.type.introductory" var="type_introductory"/>
    <fmt:message bundle="${local}" key="local.interview.type.group" var="type_group"/>
    <fmt:message bundle="${local}" key="local.interview.type.case" var="type_case"/>
    <fmt:message bundle="${local}" key="local.interview.type.technical" var="type_technical"/>
    <fmt:message bundle="${local}" key="local.interview.type.behavioral" var="type_behavioral"/>

    <fmt:message bundle="${local}" key="local.interview.status.passed" var="status_passed"/>

    <fmt:message bundle="${local}" key="local.subscriber.status.interview.appointed" var="status_appointed"/>
    <fmt:message bundle="${local}" key="local.subscriber.status.rejected" var="status_rejected"/>
    <fmt:message bundle="${local}" key="local.subscriber.status.job.offer" var="status_job_offer"/>
    <fmt:message bundle="${local}" key="local.subscriber.status.submitted" var="status_submitted"/>

    <fmt:message bundle="${local}" key="local.note.textarea.placeholder" var="note_placeholder"/>

    <title>${subscriber_control_title}</title>

</head>
<body>
<div id="wrapper">

    <%--Navigation bar--%>
    <jsp:include page="components/navigation_bar.jsp"/>

    <div class="page">

        <%--Header panel--%>
        <div class="header-panel">
            <%--Page name--%>
            <h2>${subscriber_control_title}</h2>
        </div>

        <%--Area with subscriber info--%>
        <form action="${pageContext.request.contextPath}/controller" method="post" class="forms">
            <input type="hidden" name="command" value="update_subscriber_status">
            <input type="hidden" name="subscriberId" value="${subscriberId}">

            <table>

                <thead>
                <tr id="table_header">
                    <th class="login-field">${label_login}</th>
                    <th class="name-field">${label_name} ${surname}</th>
                    <th class="phone-field">${phone}</th>
                    <th class="status-field">${status}</th>
                    <th class="actions-td">${change_status}</th>
                </tr>
                </thead>

                <tr>
                    <td class="login-field">${requestScope.user.login}</td>
                    <td class="name-field">${requestScope.user.name} ${requestScope.user.surname}</td>
                    <td class="phone-field">${requestScope.user.phoneNumber}</td>


                    <td class="status-field">
                        <select name="status" id="status_select">

                            <option value="submitted"
                                    <c:if test="${requestScope.subscriber_status eq 'SUBMITTED'}">
                                        selected</c:if> >
                                ${status_submitted}
                            </option>

                            <option value="interview_appointed"
                                    <c:if test="${requestScope.subscriber_status eq 'INTERVIEW_APPOINTED'}">
                                        selected</c:if> >
                                ${status_appointed}
                            </option>

                            <option value="job_offer"
                                    <c:if test="${requestScope.subscriber_status eq 'JOB_OFFER'}">
                                        selected</c:if> >
                                ${status_job_offer}
                            </option>

                            <option value="rejected"
                                    <c:if test="${requestScope.subscriber_status eq 'REJECTED'}">
                                        selected</c:if> >
                                ${status_rejected}
                            </option>
                        </select>
                    </td>

                    <td class="actions-td">
                        <input type="submit" value="${submit}" class="buttons" id="submit_status_btn">
                    </td>

                </tr>

            </table>
        </form>

        <%--Add new interview note button--%>
        <div class="add_btn">
            <a href="#modal_frame" class="buttons" id="add_interview_btn">${add_note_btn}</a>
        </div>

        <%--Messages area--%>
        <c:if test="${not empty requestScope.errorMessage}">
            <div class="err_messages">
                <h3><strong>${requestScope.errorMessage}</strong></h3>
            </div>
        </c:if>

        <c:if test="${empty requestScope.interviewsList}">
            <div class="err_messages">
                <h3><strong>List of interviews is empty.</strong></h3>
            </div>
        </c:if>

        <%--Table of interviews--%>
        <c:if test="${not empty requestScope.interviewsList}">
            <table class="notes_table">
                <tbody>

                <c:forEach var="interview" items="${requestScope.interviewsList}">
                    <tr>
                        <td class="type_and_status_td">
                            <span>
                                    <c:choose>
                                        <c:when test="${interview.type eq 'INTRODUCTORY'}">
                                            ${type_introductory}
                                        </c:when>
                                        <c:when test="${interview.type eq 'BEHAVIORAL'}">
                                            ${type_behavioral}
                                        </c:when>
                                        <c:when test="${interview.type eq 'TECHNICAL'}">
                                            ${type_technical}
                                        </c:when>
                                        <c:when test="${interview.type eq 'CASE'}">
                                            ${type_case}
                                        </c:when>
                                        <c:when test="${interview.type eq 'GROUP'}">
                                            ${type_group}
                                        </c:when>
                                    </c:choose>
                            </span>

                            <br>
                            <c:choose>
                                <c:when test="${interview.status eq 'APPOINTED'}">
                                    ${status_appointed}
                                </c:when>
                                <c:when test="${interview.status eq 'PASSED'}">
                                    ${status_passed}
                                </c:when>
                                <c:when test="${interview.status eq 'REJECTED'}">
                                    ${status_rejected}
                                </c:when>
                            </c:choose>
                        </td>

                        <td>
                            <div class='description_input'>
                                    ${interview.comment}
                            </div>
                        </td>

                        <td id="action_td_note">
                            <form name="deleteNote" action="${pageContext.request.contextPath}/controller"
                                  class="forms">
                                <input type="hidden" name="command" value="delete_interview"/>
                                <input type="hidden" name="interviewId" value="${interview.entityId}"/>
                                <input type="submit" value="${delete}" class="buttons" id="delete_interview_btn"/>
                            </form>
                        </td>
                    </tr>

                </c:forEach>
                </tbody>
            </table>
        </c:if>
    </div>

    <%--footer--%>
    <c:import url="components/footer.jsp"/>

    <%--wrapper--%>
</div>

<%--modal frame: ADD INTERVIEW--%>
<div class="popup">
    <a href="#x" class="overlay" id="modal_frame"></a>
    <form class="modal" id="modal_form_id" method="post" action="${pageContext.request.contextPath}/controller">
        <input type="hidden" name="command" value="add_interview">

        <h2>${new_note_title}</h2>

        <%--Interview type--%>
        <label for="type_selector" class="type">${interview_type_title}</label>
        <select name="type" id='type_selector'>
            <option value="Introductory">${type_introductory}</option>
            <option value="Behavioral">${type_behavioral}</option>
            <option value="Technical">${type_technical}</option>
            <option value="Case">${type_case}</option>
            <option value="Group">${type_group}</option>
        </select>

        <%--status--%>
        <label for="interview_status_selector" class="status">${interview_status_title}</label>
        <select name="status" id='interview_status_selector'>
            <option value="Passed">${status_passed}</option>
            <option value="Appointed">${status_appointed}</option>
            <option value="Rejected">${status_rejected}</option>
        </select>

        <%--note--%>
        <label for="note_input">${private_note_title}</label>
        <textarea name="comment" id="note_input" class="note" rows="10" cols="80" placeholder="${note_placeholder}"
                  required></textarea>

        <%--submit--%>
        <input name="submit" id="submit_btn" class="buttons" type="submit" value="${submit}"/>

    </form>
</div>

</body>

</html>