<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="custom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>

<html>
<c:set var="page" value="vacancies_list.jsp" scope="request"/>

<head>
    <meta charset="UTF-8"/>

    <!-- styles -->
    <link href="${pageContext.request.contextPath}/css/components/buttons_style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/style-form.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/vacancies_list.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/css/components/footer.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/header_panel.css" rel="stylesheet">

    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>

    <c:set var="page" value="jsp/vacancies_list.jsp" scope="session"/>

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <%--subscription statuses--%>
    <fmt:message bundle="${local}" key="local.title.vacancies.list" var="title"/>
    <fmt:message bundle="${local}" key="local.header.first.vacancies.list" var="header_text_first"/>
    <fmt:message bundle="${local}" key="local.header.second.vacancies.list" var="header_text_second"/>
    <fmt:message bundle="${local}" key="local.button.apply" var="apply_btn"/>
    <fmt:message bundle="${local}" key="local.button.add.vacancy" var="add_vacancy_btn"/>
    <fmt:message bundle="${local}" key="local.button.edit" var="edit_btn"/>
    <fmt:message bundle="${local}" key="local.button.delete" var="delete_btn"/>
    <fmt:message bundle="${local}" key="local.button.submit" var="submit_btn"/>
    <fmt:message bundle="${local}" key="local.button.cancel" var="cancel_btn"/>
    <fmt:message bundle="${local}" key="local.title.new.vacancy" var="new_vac_title"/>
    <fmt:message bundle="${local}" key="local.label.vacancy.specialization" var="vac_specialization"/>
    <fmt:message bundle="${local}" key="local.label.vacancy.location" var="vac_location"/>
    <fmt:message bundle="${local}" key="local.label.job.description" var="job_description"/>
    <fmt:message bundle="${local}" key="local.label.vacancy.status" var="vacancy_status"/>
    <fmt:message bundle="${local}" key="local.placeholder.specialization" var="plecaholder_spec"/>
    <fmt:message bundle="${local}" key="local.placeholder.location" var="plecaholder_loc"/>
    <fmt:message bundle="${local}" key="local.placeholder.description" var="plecaholder_desc"/>
    <fmt:message bundle="${local}" key="local.vacancy.status.active" var="status_active"/>
    <fmt:message bundle="${local}" key="local.vacancy.status.hidden" var="status_hidden"/>
    <fmt:message bundle="${local}" key="local.message.empty.list" var="empty_list_msg"/>

    <title>${title}</title>

</head>

<body>
<div id="wrapper">

    <%--Navigation bar--%>
    <jsp:include page="components/navigation_bar.jsp"/>

    <div class="header-image">
        <img src="${pageContext.request.contextPath}/img/header_vacancies_list.jpg" alt="header_vacancies_list.jpg"/>
    </div>

    <div class="vac-list-div">

        <div class="header-panel">
            <h2>${title}</h2>

            <c:if test="${not empty requestScope.vacanciesList}">
                <h4>${header_text_first}</h4>

                <h4>${header_text_second}</h4>
            </c:if>

            <c:if test="${empty requestScope.vacanciesList}">
                <h2>${empty_list_msg}</h2>
            </c:if>
        </div>

        <c:if test="${sessionScope.user.role eq 'HR'}">
            <div class="add_btn">
                <a href="#modal_frame" class="buttons" id="add_vac_btn">${add_vacancy_btn}</a>
            </div>
        </c:if>

        <c:if test="${not empty requestScope.vacanciesList}">
            <table class="vacancies-table">
                <tbody>
                <c:forEach var="vacancy" items="${requestScope.vacanciesList}">
                    <tr>
                        <td class="spec-loc-td"><span>${vacancy.specialization}</span><br><br>${vacancy.location}</td>
                        <td class="descr-td">${vacancy.description}</td>

                            <%--Aspirant's actions--%>
                        <c:if test="${sessionScope.user.role eq 'ASPIRANT'}">
                            <td class="actions-td">
                                <form name="ApplyForm" method="POST"
                                      action="${pageContext.request.contextPath}/controller">
                                    <input type="hidden" name="command" value="add_subscriber"/>
                                    <input type="hidden" name="vacancyId" value="${vacancy.entityId}"/>
                                    <input type="submit" value="${apply_btn}" class="buttons" id="apply_btn"/>
                                </form>
                            </td>
                        </c:if>

                            <%--Hr's actions--%>
                        <c:if test="${sessionScope.user.role eq 'HR'}">
                            <td class="actions-td">

                                <form name="EditForm" method="POST"
                                      action="${pageContext.request.contextPath}/controller">
                                    <input type="hidden" name="command" value="edit_vacancy"/>
                                    <input type="hidden" name="vacancyId" value="${vacancy.entityId}"/>
                                    <input type="submit" value="${edit_btn}" class="buttons" id="edit_btn"/>
                                </form>

                                <form name="DeleteForm" method="POST"
                                      action="${pageContext.request.contextPath}/controller">
                                    <input type="hidden" name="command" value="hide_vacancy"/>
                                    <input type="hidden" name="vacancyId" value="${vacancy.entityId}"/>
                                    <input type="submit" value="${delete_btn}" class="buttons" id="delete_btn"/>
                                </form>
                            </td>
                        </c:if>

                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <ctg:pagination command="${pageContext.request.contextPath}/controller?command=show_vacancies"
                            pageIndex="${param.pageIndex}"
                            rows="${requestScope.vacanciesAmount}"
                            count="${param.count}"/>
        </c:if>

    </div>

    <%--modal frame: ADD VACANCY--%>
    <div class="popup">
        <a href="#x" class="overlay" id="modal_frame"></a>
        <form class="modal" id="modal_form_id" method="post" action="${pageContext.request.contextPath}/controller">
            <input type="hidden" name="command" value="add_vacancy">
            <%--vacancyId only for editing, in other cases null--%>
            <input type="hidden" name="vacancyId" value="${editedVacancy.entityId}">

            <h2>${new_vac_title}</h2>
            <%--specialization--%>
            <label for="spec_input">${vac_specialization}</label>
            <input name="specialization" id="spec_input" class="spec" placeholder="${plecaholder_spec}"
                   maxlength="100" value="${editedVacancy.specialization}" required/>

            <%--location--%>
            <label for="loc_input">${vac_location}</label>
            <input name="location" id="loc_input" class="loc" placeholder="${plecaholder_loc}"
                   maxlength="100" value="${editedVacancy.location}" required/>

            <%--description--%>
            <label for="desc_input">${job_description}</label>
            <textarea name="description" id="desc_input" class="desc" rows="10" cols="80"
                      placeholder="${plecaholder_desc}"
                      required>${editedVacancy.description}</textarea>

            <%--status--%>
            <label for="status_selector" class="status">${vacancy_status}</label>
            <select name="status" id='status_selector'>
                <option value="0" <c:if test="${editedVacancy.isActive == 0}">selected</c:if>>${status_hidden}</option>
                <option value="1" <c:if test="${editedVacancy.isActive == 1}">selected</c:if>>${status_active}</option>
            </select>

            <%--Modal frame buttons--%>
            <c:if test="${empty editedVacancy}">
                <input id="submit_btn" class="buttons" type="submit" value="${submit_btn}"/>
            </c:if>

            <c:if test="${not empty editedVacancy}">
                <input id="submit_btn_alt" class="buttons" type="submit" value="${submit_btn}"/>
                <a href="<c:remove var="editedVacancy"/>
                ${pageContext.request.contextPath}/controller?command=show_vacancies" >
                    <button id="cancel_btn_alt" class="buttons" type="button">${cancel_btn}</button>
                </a>
            </c:if>

        </form>
    </div>

    <%--footer--%>
    <c:import url="components/footer.jsp"/>

    <%--wrapper--%>
</div>
</body>
</html>