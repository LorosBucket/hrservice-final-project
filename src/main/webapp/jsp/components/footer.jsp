<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <fmt:message bundle="${local}" key="local.footer.contact" var="contact"/>
    <fmt:message bundle="${local}" key="local.footer.copyright" var="copyright"/>
</head>
<body>

<div id="separator"></div>

<div id="footer">
    <div class="copyright">
        &copy; 2018 <span id="logo">Hr<span>Service</span></span>. ${copyright}
    </div>
    <div class="contact">
        ${contact} Lorosmail&#64yandex.ru
    </div>
</div>
</body>
</html>
