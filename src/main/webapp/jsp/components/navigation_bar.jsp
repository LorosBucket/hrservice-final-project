<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/components/navigation_bar.css">

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <fmt:message bundle="${local}" key="local.navigation.vacancies" var="vacancies"/>
    <fmt:message bundle="${local}" key="local.navigation.applications" var="applications"/>
    <fmt:message bundle="${local}" key="local.navigation.logout" var="logout"/>
    <fmt:message bundle="${local}" key="local.navigation.responses" var="responses"/>
    <fmt:message bundle="${local}" key="local.navigation.users" var="users"/>
    <fmt:message bundle="${local}" key="local.navigation.aspirants" var="aspirants"/>
    <fmt:message bundle="${local}" key="local.navigation.hrs" var="hrs"/>

</head>

<div class="nav-body">

    <div class="topnav">

        <div class="header_logo">
            <div>Hr<span>Service</span></div>
        </div>

        <a href="${pageContext.request.contextPath}/controller?command=show_vacancies">${vacancies}</a>

        <%--Aspirant's menu--%>
        <c:if test="${sessionScope.user.role eq 'ASPIRANT'}">
            <a href="${pageContext.request.contextPath}/jsp/aspirant_home_page.jsp">${applications}</a>
        </c:if>


        <%--HR's menu--%>
        <c:if test="${sessionScope.user.role eq 'HR'}">
            <a href="${pageContext.request.contextPath}/controller?command=show_vacancies_with_responses">${responses}</a>
        </c:if>

        <%--Admin's menu--%>
        <c:if test="${sessionScope.user.role eq 'ADMIN'}">
            <div class="dropdown">
                <button class="dropbtn">${users}
                    <span class="list-symbol"> &#9660</span>
                </button>
                <div class="dropdown-content">
                    <a href="${pageContext.request.contextPath}/controller?command=show_aspirants_list">${aspirants}</a>
                    <a href="${pageContext.request.contextPath}/controller?command=show_hrs_list">${hrs}</a>
                </div>
            </div>
        </c:if>

        <form name="LogoutForm" action="${pageContext.request.contextPath}/controller" method="post" class="nav-form">
            <input type="hidden" name="command" value="logout"/>
            <button type="submit" name="logout" class="logout_btn">${logout}</button>
        </form>

        <div class="dropdown-right">

            <button class="dropbtn-right">
                ${sessionScope.local}
                <span class="list-symbol"> &#9660</span>
            </button>

            <div class="dropdown-content-right">
                <form class="lang_form" action="${pageContext.request.contextPath}/controller" method="post">
                    <input name="command" value="change_local" type="hidden">

                    <c:if test="${sessionScope.local ne 'ru'}">
                        <button type="submit" class="lang_btn" name="local" value="ru">RU</button>
                    </c:if>

                    <c:if test="${sessionScope.local ne 'en'}">
                        <button type="submit" class="lang_btn" name="local" value="en">EN</button>
                    </c:if>

                    <c:if test="${sessionScope.local ne 'es'}">
                        <button type="submit" class="lang_btn" name="local" value="es">ES</button>
                    </c:if>
                </form>
            </div>

        </div>
    </div>
</div>

</body>
</html>

