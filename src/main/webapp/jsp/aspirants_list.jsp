<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">

    <link href="${pageContext.request.contextPath}/css/components/footer.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/users_table_style.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/components/buttons_style.css" rel="stylesheet">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>
    <link href="${pageContext.request.contextPath}/css/components/header_panel.css" rel="stylesheet">

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <%--subscription statuses--%>
    <fmt:message bundle="${local}" key="local.title.aspirants" var="aspirants"/>
    <fmt:message bundle="${local}" key="local.name" var="label_name"/>
    <fmt:message bundle="${local}" key="local.surname" var="surname"/>
    <fmt:message bundle="${local}" key="local.phone.number" var="phone"/>
    <fmt:message bundle="${local}" key="local.status" var="status"/>
    <fmt:message bundle="${local}" key="local.actions" var="actions"/>
    <fmt:message bundle="${local}" key="local.block" var="block"/>
    <fmt:message bundle="${local}" key="local.unblock" var="unblock"/>
    <fmt:message bundle="${local}" key="local.login" var="label_login"/>
    <fmt:message bundle="${local}" key="local.message.empty.list" var="empty_list_msg"/>

    <title>${aspirants}</title>

</head>

<body>
<div id="wrapper">


    <%--Navigation bar--%>
    <jsp:include page="components/navigation_bar.jsp"/>

    <c:set var="currentPage" value="jsp/aspirants_list.jsp" scope="session"/>

    <%--<c:if test="${empty requestScope.aspirantsList}">--%>
    <%--<jsp:include command="${requestScope.contextPath}/controller">--%>
    <%--<jsp:param name="command" value="get_aspirants_list"/>--%>
    <%--</jsp:include>--%>
    <%--</c:if>--%>

    <div class="header-image">
        <img src="${pageContext.request.contextPath}/img/header_aspirants.jpeg" alt="header.jpg"/>
    </div>

    <div class="page">


        <div class="header-panel">
            <h2>${aspirants}</h2>
            <c:if test="${empty requestScope.aspirantsList}">
                <h2><br><strong>${empty_list_msg}</strong></h2>
            </c:if>
        </div>

        <c:if test="${not empty requestScope.aspirantsList}">
            <table class="users-table">
                <thead>
                <tr>
                    <th class="login-field">${label_login}</th>
                    <th class="name-field">${label_name} ${surname}</th>
                    <th class="phone-field">${phone}</th>
                    <th class="status-field">${status}</th>
                    <th class="actions-td">${actions}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="subscriber" items="${requestScope.aspirantsList}">
                    <tr>
                        <td class="login-field">${subscriber.login}</td>
                        <td class="name-field">${subscriber.name} ${subscriber.surname}</td>
                        <td class="phone-field">${subscriber.phoneNumber}</td>
                        <td class="status-field">

                            <c:choose>
                                <c:when test="${subscriber.isBlocked == true}">
                                    <img src="${pageContext.request.contextPath}/img/false.png" alt="blocked" width="30"
                                         height="30">
                                </c:when>
                                <c:when test="${subscriber.isBlocked == false}">
                                    <img src="${pageContext.request.contextPath}/img/true.png" alt="unblocked"
                                         width="30" height="30">
                                </c:when>
                            </c:choose>

                        </td>

                        <td class="actions-td">
                            <c:choose>
                                <c:when test="${subscriber.isBlocked == true}">
                                    <form name="UnblockAccountForm" method="POST"
                                          action="${pageContext.request.contextPath}/controller">
                                        <input type="hidden" name="command" value="unblock_account"/>
                                        <input type="hidden" name="userId" value="${subscriber.entityId}"/>
                                        <input type="hidden" name="commandPage"
                                               value="controller?command=show_aspirants_list">
                                        <input type="submit" value="${unblock}" class="buttons" id="unblock_btn"/>
                                    </form>
                                </c:when>
                                <c:when test="${subscriber.isBlocked == false}">
                                    <form name="BlockAccountForm" method="POST"
                                          action="${pageContext.request.contextPath}/controller">
                                        <input type="hidden" name="command" value="block_account"/>
                                        <input type="hidden" name="userId" value="${subscriber.entityId}"/>
                                        <input type="hidden" name="commandPage"
                                               value="controller?command=show_aspirants_list">
                                        <input type="submit" value="${block}" class="buttons" id="block_btn"/>
                                    </form>
                                </c:when>
                            </c:choose>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>

    </div>

    <%--footer--%>
    <c:import url="components/footer.jsp"/>

    <%--wrapper--%>
</div>
</body>
</html>