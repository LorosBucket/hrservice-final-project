<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>

<html>

<head>
    <c:set var="page" value="/controller?command=show_aspirant_vacancies" scope="session"/>
</head>

<body>
<jsp:forward page="/controller">
    <jsp:param name="command" value="show_aspirant_vacancies"/>
</jsp:forward>
</body>
</html>