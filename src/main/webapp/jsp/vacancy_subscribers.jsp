<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">

    <%--styles--%>
    <link href="${pageContext.request.contextPath}/css/components/buttons_style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/users_table_style.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/components/footer.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/components/header_panel.css" rel="stylesheet">

    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico"/>

    <c:set var="page" value="jsp/vacancy_subscribers.jsp" scope="session"/>

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <fmt:message bundle="${local}" key="local.title.vacancy.subscribers" var="vacancy_subscribers_title"/>
    <fmt:message bundle="${local}" key="local.button.control" var="control_btn"/>

    <fmt:message bundle="${local}" key="local.login" var="label_login"/>
    <fmt:message bundle="${local}" key="local.name" var="label_name"/>
    <fmt:message bundle="${local}" key="local.surname" var="label_surname"/>
    <fmt:message bundle="${local}" key="local.phone.number" var="label_phone"/>
    <fmt:message bundle="${local}" key="local.status" var="label_status"/>
    <fmt:message bundle="${local}" key="local.actions" var="label_actions"/>

    <fmt:message bundle="${local}" key="local.subscriber.status.interview.appointed" var="status_appointed"/>
    <fmt:message bundle="${local}" key="local.subscriber.status.rejected" var="status_rejected"/>
    <fmt:message bundle="${local}" key="local.subscriber.status.job.offer" var="status_job_offer"/>
    <fmt:message bundle="${local}" key="local.subscriber.status.submitted" var="status_submitted"/>

    <title>${vacancy_subscribers_title}</title>

</head>
<body>
<div id="wrapper">

    <%--Navigation bar--%>
    <jsp:include page="components/navigation_bar.jsp"/>

    <c:if test="${not empty requestScope.subscriptionsDtoList}">
        <div class="page">

            <div class="header-panel">
                <h2>${vacancy_subscribers_title}</h2>
            </div>

            <table class="users-table">
                <thead>
                <tr>
                    <th class="login-field">${label_login}</th>
                    <th class="name-field">${label_name} ${label_surname}</th>
                    <th class="phone-field">${label_phone}</th>
                    <th class="status-field">${label_status}</th>
                    <th class="actions-td">${label_actions}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="subscriber" items="${requestScope.subscriptionsDtoList}">
                    <tr>
                        <td class="login-field">${subscriber.login}</td>
                        <td class="name-field">${subscriber.name} ${subscriber.surname}</td>
                        <td class="phone-field">${subscriber.phoneNumber}</td>
                        <td class="status-field">

                            <c:choose>
                                <c:when test="${subscriber.status eq 'SUBMITTED'}">
                                    ${status_submitted}
                                </c:when>
                                <c:when test="${subscriber.status eq 'INTERVIEW_APPOINTED'}">
                                    ${status_appointed}
                                </c:when>
                                <c:when test="${subscriber.status eq 'JOB_OFFER'}">
                                    ${status_job_offer}
                                </c:when>
                                <c:when test="${subscriber.status eq 'REJECTED'}">
                                    ${status_rejected}
                                </c:when>
                            </c:choose>

                        </td>
                        <td class="actions-td">

                            <form name="showInterviewNotes" action="${pageContext.request.contextPath}/controller">
                                <input type="hidden" name="command" value="subscriber_control"/>
                                <input type="hidden" name="subscriberId" value="${subscriber.subscriberId}"/>
                                <input type="hidden" name="userId" value="${subscriber.userId}"/>

                                <input type="submit" value="${control_btn}" class="buttons" id="control_btn"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </c:if>

    <%--footer--%>
    <c:import url="components/footer.jsp"/>

    <%--wrapper--%>
</div>
</body>
</html>
