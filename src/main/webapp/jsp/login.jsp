<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>HrService</title>

    <%--styles--%>
    <link href="${pageContext.request.contextPath}/css/components/style-form.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/login_st.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="favicon.ico"/>

    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="local" var="local"/>

    <fmt:message bundle="${local}" key="local.login" var="field_login"/>
    <fmt:message bundle="${local}" key="local.password" var="field_password"/>
    <fmt:message bundle="${local}" key="local.input" var="input"/>

    <c:if test="${not empty sessionScope.errorLoginPassMessage}">
        <fmt:message bundle="${local}" key="${sessionScope.errorLoginPassMessage}" var="message"/>
    </c:if>

    <c:if test="${not empty sessionScope.errorSignUpMessage}">
        <fmt:message bundle="${local}" key="${sessionScope.errorSignUpMessage}" var="sign_up_message"/>
    </c:if>

    <fmt:message bundle="${local}" key="local.name" var="field_name"/>
    <fmt:message bundle="${local}" key="local.surname" var="field_surname"/>
    <fmt:message bundle="${local}" key="local.phone.number" var="field_phone"/>
    <fmt:message bundle="${local}" key="local.button.cancel" var="cancel_btn"/>
    <fmt:message bundle="${local}" key="local.button.sign.up" var="sign_up_btn"/>
    <fmt:message bundle="${local}" key="local.title.sign.up" var="title_sign_up"/>
    <fmt:message bundle="${local}" key="local.label.login" var="label_login"/>
    <fmt:message bundle="${local}" key="local.label.name" var="label_name"/>
    <fmt:message bundle="${local}" key="local.label.surname" var="label_surname"/>
    <fmt:message bundle="${local}" key="local.label.phone.number" var="label_phone_number"/>
    <fmt:message bundle="${local}" key="local.label.password" var="label_password"/>
    <fmt:message bundle="${local}" key="local.label.password.again" var="label_password_again"/>

    <script src="${pageContext.request.contextPath}/js/login.js"></script>
</head>

<body>
<div class="ground"></div>

<div class="grad"></div>

<div class="lang">
    <form class="lang_form" action="${pageContext.request.contextPath}/controller" method="post">
        <input name="command" value="change_local" type="hidden">
        <button type="submit" class="lang_btn" name="local" value="ru">RU</button>
        &#124
        <button type="submit" class="lang_btn" name="local" value="en">EN</button>
        &#124
        <button type="submit" class="lang_btn" name="local" value="es">ES</button>
    </form>

</div>

<div class="header">
    <div>Hr<span>Service</span></div>
</div>

<div class="login">
    <form name="LoginForm" action="${pageContext.request.contextPath}/controller" method="POST">
        <input type="hidden" name="command" value="login"/>
        <input type="text" name="login" value="" placeholder="${field_login}" required/>
        <input type="password" name="password" value="" placeholder="${field_password}" required/>
        <input type="submit" value="${input}"/>
    </form>
    <div class="err_messages">
        ${message}
    </div>

    <div class="sign_up">
        <a href="#modal_frame" class="buttons" id="sign_up_btn">${sign_up_btn}</a>
    </div>
</div>

<%--modal frame: Sign up--%>
<div class="popup">
    <a href="#x" class="overlay" id="modal_frame"></a>
    <form class="modal" id="modal_form_id" method="post" action="${pageContext.request.contextPath}/controller">
        <input type="hidden" name="command" value="registration">

        <%--title--%>
        <h2 id="frame_title">${title_sign_up}</h2>

        <%--login--%>
        <div class="block">
            <label for="login_input">${label_login}</label>
            <input type="text" name="login" id="login_input" class="login_input" placeholder="${field_login}"
                   minlength="2" maxlength="20" value="${sessionScope.newUser.login}" required/>
        </div>

        <%--name--%>
        <div class="block">
            <label for="name_input">${label_name}</label>
            <input type="text" name="name" id="name_input" class="name_input" placeholder="${field_name}"
                   maxlength="100" value="${sessionScope.newUser.name}" required/>
        </div>

        <%--surname--%>
        <div class="block">
            <label for="surname_input">${label_surname}</label>
            <input type="text" name="surname" id="surname_input" class="surname_input" placeholder="${field_surname}"
                   maxlength="100" value="${sessionScope.newUser.surname}" required/>
        </div>

        <%--phone number--%>
        <div class="block">
            <label for="phone_input">${label_phone_number}</label>
            <input type="text" name="phone" id="phone_input" class="phone_input" placeholder="+375001234567"
                   minlength="13" maxlength="13" value="${sessionScope.newUser.phoneNumber}" pattern="^\+[0-9]{12}" required/>
        </div>

        <%--password--%>
        <div class="block">
            <div class="inner_block">
                <label for="pass_input">${label_password}</label>
                <input type="password" name="pass" id="pass_input" class="pass_input" placeholder="${field_password}"
                       minlength="4" maxlength="50" required
                       onkeypress="CorrectPass()" onfocus="CorrectPass()" onkeyup="CorrectPass()"/>
            </div>

            <%--password again--%>
            <div class="inner_block">
                <label for="pass_again_input">${label_password_again}</label>
                <input type="password" name="pass_again" id="pass_again_input" class="pass_input_again"
                       placeholder="${field_password}" minlength="4" maxlength="50" required
                       onkeypress="CorrectPass()" onfocus="CorrectPass()" onkeyup="CorrectPass()"/>
            </div>

        </div>

        <div class="message">
            <div id="repass_correct"></div>
            ${sign_up_message}
        </div>

        <%--Modal frame buttons--%>
        <a href="${pageContext.request.contextPath}/index.jsp">
            <button id="cancel_btn" class="buttons" type="button">${cancel_btn}</button>
        </a>

        <input id="submit_btn" class="buttons" type="submit" value="${sign_up_btn}"/>

        <%--Fiels for password validation script--%>
        <input type="hidden" name="check_repass" id="check_repass" value="0"/>
        <input type="hidden" name="check" id="check" value="0"/>
        <input type="hidden" name="lang" id="lang" value="${sessionScope.local}"/>

    </form>
</div>

</body>
</html>
