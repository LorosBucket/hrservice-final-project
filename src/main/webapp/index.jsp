<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>

<html>

<head>
    <title>Index</title>
    <c:set var="page" value="index.jsp" scope="session"/>
    <link rel="icon" href="favicon.ico"/>

</head>

<body>
<jsp:forward page="jsp/login.jsp"/>
</body>

</html>
