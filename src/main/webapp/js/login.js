function CorrectPass() {
    var item_pass_value = document.getElementById('pass_input').value;
    var item_pass_again_value = document.getElementById('pass_again_input').value;
    var item_pass_length = document.getElementById('pass_input').value.length;
    var item_pass_again_length = document.getElementById('pass_again_input').value.length;
    var item_correct = 'repass_correct';

    var lang = document.getElementById('lang').value.toString();

    var error_msg = 'Пароли не совпадают';
    var short_pass = 'Слишком короткий пароль';

    if (lang == 'en') {
        short_pass = 'Too short password';
        error_msg = 'Do not match';
    }  else if (lang == 'es') {
        error_msg = 'Las contraseñas no coinciden';
        short_pass = 'Demasiado corta una contraseña';
    }

    if (item_pass_length > 0 && item_pass_again_length > 0){
        if (item_pass_length < 4 || item_pass_again_length < 4){
            document.getElementById(item_correct).innerHTML = short_pass;
            document.getElementById(item_correct).className = 'acorrect';
            document.getElementById('check_repass').value = 0;
        } else if (item_pass_value == item_pass_again_value){
            document.getElementById(item_correct).innerHTML = '';
            document.getElementById(item_correct).className = 'correct';
            document.getElementById('check_repass').value = 1;
        } else {
            document.getElementById(item_correct).innerHTML = error_msg;
            document.getElementById(item_correct).className = 'acorrect';
            document.getElementById('check_repass').value = 0;
        }
        check();
    }
}

function check() {
    var check_repass = document.getElementById('check_repass').value;
    var x = check_repass;
    document.getElementById('check').value = x;

    if (document.getElementById('check').value == 1) {
        document.getElementById('submit_btn').disabled = false;
    } else {
        document.getElementById('submit_btn').disabled = true;
    }
}