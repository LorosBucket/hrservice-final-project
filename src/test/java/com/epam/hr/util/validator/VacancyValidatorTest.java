package com.epam.hr.util.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class VacancyValidatorTest {

    private static final String VALID_SPEC = "Javascript Developer";
    private static final String VALID_LOC = "MOSCOW, RUSSIA";
    private static final String VALID_DESC = "Lorem Ipsum is simply dummy text of the printing and" +
            " typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since" +
            " the 1500s, when an unknown printer took a galley of type and scrambled it to make a type " +
            "specimen book. It has survived not only five centuries, but also the leap into electronic " +
            "typesetting, remaining essentially unchanged.";
    private static final byte VALID_ACTIVE_STATUS = 1;

    private static final String EMPTY_SPEC = "";
    private static final String EMPTY_LOC = "";
    private static final String EMPTY_DESC = "";

    private static final String INVALID_LONG_SPEC = "ThisTextConsistsOf124CharactersThisTextConsistsOf124" +
            "CharactersThisTextConsistsOf124CharactersThisTextConsistsOf124Characters";
    private static final String INVALID_LONG_LOC = "ThisTextConsistsOf124CharactersThisTextConsistsOf124" +
            "CharactersThisTextConsistsOf124CharactersThisTextConsistsOf124Characters";
    private static final String INVALID_LONG_DESC = "This text consists of 1100 characters. Contrary to" +
            " popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical" +
            " Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin" +
            " professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words," +
            " consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical" +
            " literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and" +
            " 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero," +
            " written in 45 BC. This book is a treatise on the theory of ethics, very popular during the " +
            "Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes frome a" +
            " line in section 1.1.0.32.\n" +
            "\n" +
            "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested." +
            " Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced" +
            " in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.";
    private static final byte INVALID_STATUS = 2;

    @Test
    public void shouldTrueWhenValidParams(){
        boolean actual = VacancyValidator.validate(VALID_SPEC, VALID_LOC, VALID_DESC, VALID_ACTIVE_STATUS);
        Assert.assertTrue(actual);
    }

    @Test
    public void shouldFalseWhenEmptyParams(){
        boolean actual = VacancyValidator.validate(EMPTY_SPEC, EMPTY_LOC, EMPTY_DESC, VALID_ACTIVE_STATUS);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenInvalidLongParams(){
        boolean actual = VacancyValidator.validate(INVALID_LONG_SPEC, INVALID_LONG_LOC, INVALID_LONG_DESC, INVALID_STATUS);
        Assert.assertFalse(actual);
    }
}