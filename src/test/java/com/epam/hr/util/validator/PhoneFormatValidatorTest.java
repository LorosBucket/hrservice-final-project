package com.epam.hr.util.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PhoneFormatValidatorTest {

    private static final String VALID = "+375336666040";
    private static final String INVALID_WITHOUT_PLUS = "375336666040";
    private static final String INVALID_SHORT = "37533040";
    private static final String INVALID_WITH_CHARACTERS = "+375..6666O4O";

    @Test
    public void shouldTrueWhenValid() {
        boolean actual = PhoneFormatValidator.validate(VALID);
        Assert.assertTrue(actual);
    }

    @Test
    public void shouldFalseWhenInvalidWithoutPlus() {
        boolean actual = PhoneFormatValidator.validate(INVALID_WITHOUT_PLUS);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenInvalidShort() {
        boolean actual = PhoneFormatValidator.validate(INVALID_SHORT);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenInvalidWithChars() {
        boolean actual = PhoneFormatValidator.validate(INVALID_WITH_CHARACTERS);
        Assert.assertFalse(actual);
    }
}