package com.epam.hr.util.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginValidatorTest {

    private static final String VALID_LOGIN = "aLogin77";
    private static final String INVALID_WITH_WHITESPACE = "THE LOGIN";
    private static final String EMPTY_LOGIN = "";
    private static final String INVALID_SHORT_LOGIN = "1";
    private static final String INVALID_LONG_LOGIN = "ThisLoginConsistsOf31Characters";

    @Test
    public void shouldTrueWhenValidLogin() {
        boolean actual = LoginValidator.validate(VALID_LOGIN);
        Assert.assertTrue(actual);
    }

    @Test
    public void shouldFalseWhenInvalidWithWhitespace() {
        boolean actual = LoginValidator.validate(INVALID_WITH_WHITESPACE);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenEmptyLogin() {
        boolean actual = LoginValidator.validate(EMPTY_LOGIN);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenInvalidShort() {
        boolean actual = LoginValidator.validate(INVALID_SHORT_LOGIN);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenInvalidLong() {
        boolean actual = LoginValidator.validate(INVALID_LONG_LOGIN);
        Assert.assertFalse(actual);
    }
}