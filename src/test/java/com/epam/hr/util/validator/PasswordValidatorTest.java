package com.epam.hr.util.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PasswordValidatorTest {

    private static final String VALID_PASSWORD = "pASsWOrD77";
    private static final String INVALID_WITH_WHITESPACE = "THE password";
    private static final String EMPTY_PASSWORD = "";
    private static final String INVALID_SHORT_PASSWORD = "1";
    private static final String INVALID_LONG_PASSWORD = "ThisPasswConsistsOf62CharactersThisPasswConsistsOf62Characters";

    @Test
    public void shouldTrueWhenValidPass() {
        boolean actual = PasswordValidator.validate(VALID_PASSWORD);
        Assert.assertTrue(actual);
    }

    @Test
    public void shouldFalseWhenInvalidWithWhitespace() {
        boolean actual = PasswordValidator.validate(INVALID_WITH_WHITESPACE);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenEmptyPass() {
        boolean actual = PasswordValidator.validate(EMPTY_PASSWORD);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenInvalidShort() {
        boolean actual = PasswordValidator.validate(INVALID_SHORT_PASSWORD);
        Assert.assertFalse(actual);
    }

    @Test
    public void shouldFalseWhenInvalidLong() {
        boolean actual = PasswordValidator.validate(INVALID_LONG_PASSWORD);
        Assert.assertFalse(actual);
    }
}